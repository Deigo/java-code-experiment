# java-code-experim

#### 介绍
   大三下期的java程序设计课程的代码

#### 代码结构

  > 1. experim1 ~ experim 13 为实验1~实验13代码
  > 2. day1 ~ day3 为企业实训三天代码
  > 3. day4 为Java高级编程 课程考察报告代码
  > 4. comment 为评价对象的代码



#### 安装教程

1.  xxxx

#### 使用说明

1.  jdk版本是1.8
2. 本项目采用的数据库是Java内置的Derby数据库https://db.apache.org/derby/releases/release-10_14_2_0.cgi
3. 运行day1~day3前需运行derby数据库的网络环境，即运行bin目录下的startNetworkServer

#### 参与贡献

1.  mars


#### 特技
