package com.mars.experim6.problem1.tom2.jierui2;

import com.mars.experim6.problem1.tom1.jierui1.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        //在主类中，通过Student创建一个数组，有两个数组元素：分别为“张一”、20岁；“王二”、19岁；
        List<Student> students = new ArrayList<>();
        students.add(new Student("张一", 20));
        students.add(new Student("王二", 19));
        //通过命令行输入学生的年龄，来查看该年龄都有哪些同学。
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入查看学生的年龄：");
        int age = scanner.nextInt();
        Student.showStusByAge(students, age);
    }
}
