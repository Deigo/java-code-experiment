package com.mars.experim6.problem1.tom1.jierui1;

import java.util.List;

public class Student {
    private String name;
    private int age;

    public Student() {

    }
    public Student(String name) {
        this.name = name;
    }

    public Student(int age) {
        this.age = age;
    }

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // 根据年龄查看学生
    public static void showStusByAge(List<Student> stus,int age){
        System.out.println("年龄在"+age+"岁的学生如下：");
        stus.forEach(student -> {
           if (student.getAge() == age){
               System.out.println(student.getName());
           }
        });
    }
}
