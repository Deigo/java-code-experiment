package com.mars.experim12.problem2;

import javax.swing.*;

public class WordThread extends Thread{
    char word;  //声明显示字符
    int startPosition = 19968; //声明字符起始位置
    int endPosition = 32320; //声明字符截止位置
    JTextField showWord; // 声明字符显示框
    int sleepLength; // 声明失眠时间

    //设置字符显示框
    public void setJTextField(JTextField showWord) {
        this.showWord = showWord;
        showWord.setEditable(false); //并设置为不可编辑
    }

    //设置睡眠时间
    public void setSleepLength(int sleepLength) {
        this.sleepLength = sleepLength;
    }

    @Override
    public void run() {
        int k = startPosition; //初始化当前进程字符
        while (true){
            word = (char) k;  //将当前字符由int强转为char，即由数字转化为汉字
            showWord.setText(""+word); //将当前汉字设置在显示框上
            try {
                sleep(sleepLength); //睡眠sleepLength毫秒
            } catch (InterruptedException e) { //捕获阻塞中断异常
                e.printStackTrace();
            }
            k++; //字符加1，即切换到下一个汉字
            if (k>=endPosition){ //如果到了汉字的最后一个，则回到开始位置
                k=startPosition;
            }
        }
    }
}
