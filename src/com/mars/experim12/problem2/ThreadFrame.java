package com.mars.experim12.problem2;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ThreadFrame extends JFrame implements ActionListener {
    JTextField showWord; //声明汉字显示框
    JButton button; // 声明一个按钮
    JTextField inputText,showScore; //声明输入框，和计分框
    WordThread giveWord; //声明一个汉字显示的进程
    int score = 0;
    public ThreadFrame() throws HeadlessException {
        showWord = new JTextField(6); //给输入框创建一个文本框对象，长度为6，
        showWord.setFont(new Font("",Font.BOLD,72));//设置输入框内的字体加粗,大小为72
        showWord.setHorizontalAlignment(JTextField.CENTER); //设置文本的对其方式为居中对齐
        giveWord = new WordThread();      //给giveWord创建一个WordThread对象
        giveWord.setJTextField(showWord); //设置giveWord的汉字显示框为showWord
        giveWord.setSleepLength(5000);  //设置睡眠时间为5s，即每个汉字的切换间隔时间为5s
        button = new JButton("开始"); //给button创建一个按钮对象，按钮上显示开始
        inputText = new JTextField(10);//给输入框创建文本框对象，长度为10
        showScore = new JTextField(5);// 给积分狂创建文本框对象，长度为5
        showScore.setEditable(false);   //设置计分框不可编辑
        button.addActionListener(this); //按钮添加当前类为监听类
        inputText.addActionListener(this);//输入框添加当前类为监听类
        add(button,BorderLayout.NORTH); //画板采用BorderLayout，将按钮添加到画板的北侧
        add(showWord,BorderLayout.CENTER);//将文字显示框添加到画板的中心位置
        JPanel southP = new JPanel(); //创建一个个通用的轻量级容器对象，对象名为southP
        southP.add(new JLabel("输入汉字（回车）："));//在southP中添加一个标签，内容是"输入汉字（回车）："
        southP.add(inputText);//把输入框添加到southP中
        southP.add(showScore);//把积分框添加到southP中
        add(southP,BorderLayout.SOUTH);//将southP添加到画板南侧
        setBounds(100,100,350,180); //设置画板的左上角位置是100,100，宽为350，高为180
        setVisible(true); //设置画板为可见
        validate(); //验证此容器及其所有子组件。
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序
    }

    //实现ActionListener中的actionPerformed接口
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button){//如果用户点击按钮
            if (!(giveWord.isAlive())){//如果显示汉字的进程giveWord没有开启
                giveWord = new WordThread(); //给giveWord创建一个WordThread对象
                giveWord.setJTextField(showWord);//设置giveWord的汉字显示框为showWord
                giveWord.setSleepLength(5000);//设置睡眠时间为5s，即每个汉字的切换间隔时间为5s
            }
            try {
                giveWord.start();//启动giveWord进程
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }else if (e.getSource() == inputText){//如果是输入框
            if (inputText.getText().equals(showWord.getText())){//如果输入框中的字符与显示的字符相同
                score++;//得分加1
            }
            showScore.setText("得分："+score);//更新得分
            inputText.setText(null);//清空输入框
        }
    }
}
