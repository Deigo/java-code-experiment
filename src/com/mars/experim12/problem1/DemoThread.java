package com.mars.experim12.problem1;

import static java.lang.Thread.sleep;

public class DemoThread implements Runnable{

    private Integer count = null;

    public DemoThread(Integer count) {
        this.count = count;
    }

    @Override
    public void run() {
        while (true){
            if (Thread.currentThread().getName().equals("raise")){
                if (!changeNum(true))
                    break;
            }else if (Thread.currentThread().getName().equals("reduce")){
                if (!changeNum(false))
                    break;
            }
        }

    }

    private  boolean changeNum(Boolean flag) {
        if (flag == true) {
            if (this.count >= 300)
                return false;
            this.count++;
            System.out.println("增加1，当前数字为：" + this.count);
            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            if (this.count <= 0)
                return false;
            this.count--;
            System.out.println("减少1，当前数字为：" + this.count);
            try {
                sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return  true;
    }
}
