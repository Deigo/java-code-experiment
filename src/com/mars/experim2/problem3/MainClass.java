package com.mars.experim2.problem3;

import com.mars.experim2.problem3.Area;

public class MainClass {
    public static void main(String[] args) {
        Area area = new Area();
        double rectangleArea = area.getRectangleArea(25, 20);
        System.out.println("长为25，宽为20的矩形面积为：" + rectangleArea);
        double tixingArea = area.getTixingArea(10, 12, 8);
        System.out.println("上底为10，下底为12，高为8的梯形面积为：" + tixingArea);
    }
}
