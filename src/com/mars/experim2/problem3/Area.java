package com.mars.experim2.problem3;


public class Area {
    //计算矩形的面积
    double getRectangleArea(double length,double width){
        return  length*width;
    }
    //计算梯形的面积
    double getTixingArea(double shangdi,double xiadi,double height){
        return (shangdi+xiadi)*height/2;
    }
}
