package com.mars.experim8.problem1;

public class E {

    public static void main(String[] args) {
        Dog yellowDog = new Dog();
        yellowDog.setState(new SoftlyState());
        yellowDog.cry();
        yellowDog.setState(new MeetEnemyState());
        yellowDog.cry();
        yellowDog.setState(new MeetFriendState());
        yellowDog.cry();
    }
}
