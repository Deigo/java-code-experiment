package com.mars.experim8.problem1;

interface DogState{
    public void showState();
}

class SoftlyState implements DogState{
    @Override
    public void showState() {
        System.out.println("现在很温顺");
    }
}

class MeetEnemyState implements DogState{
    @Override
    public void showState() {
        System.out.println("遇到敌人狂叫，并冲上去狠咬敌人");
    }
}

class MeetFriendState implements DogState{
    @Override
    public void showState() {
        System.out.println("遇到朋友很高兴，并冲上去打招呼");
    }
}

class Dog{
    DogState state;
    public void cry(){
        state.showState();
    }
    public void setState(DogState state) {
        this.state = state;
    }
}

