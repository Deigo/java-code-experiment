package com.mars.experim8.problem2;

public class MainClass {
    public static void main(String[] args) {
        //请分别播报晴天、下雨、雪天的天气状况。
        Weather weather = new Weather();
        weather.setWeatherForecast( new Sunny());
        weather.broadcastWeather();
        weather.setWeatherForecast( new Raining());
        weather.broadcastWeather();
        weather.setWeatherForecast( new Snowy());
        weather.broadcastWeather();
    }
}
