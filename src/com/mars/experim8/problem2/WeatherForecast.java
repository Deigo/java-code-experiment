package com.mars.experim8.problem2;

public interface WeatherForecast {
    //播报天气预报
    public void broadcastWeather();
}

//晴天、
class Sunny implements WeatherForecast{
    @Override
    public void broadcastWeather() {
        System.out.println("Today is Sunny");
    }
}
// 下雨、
class Raining implements WeatherForecast{
    @Override
    public void broadcastWeather() {
        System.out.println("Today is Raining");
    }
}
// 雪天
class Snowy implements WeatherForecast{
    @Override
    public void broadcastWeather() {
        System.out.println("Today is Snowy");
    }
}

class Weather{
    WeatherForecast weatherForecast;
    public void broadcastWeather(){
        this.weatherForecast.broadcastWeather();
    }

    public void setWeatherForecast(WeatherForecast weatherForecast) {
        this.weatherForecast = weatherForecast;
    }
}
