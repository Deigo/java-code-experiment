package com.mars.last.test;

import com.mars.last.config.DerbyConfig;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class TestClass {
    public static void main(String[] args) {
//        MySQLConfig mySQLConfig = new MySQLConfig();
//        Connection connection = mySQLConfig.getConnection();
//        String  sql= "select * from student";
//        try {
//            Statement statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery(sql);
//            while (resultSet.next()){
//                String id = resultSet.getString("id");
//                String name = resultSet.getString("name");
//                String phone = resultSet.getString("phone");
//                System.out.println(id+":"+name+":"+phone);
//            }
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
        DerbyConfig derbyConfig = new DerbyConfig();
        Connection connection = derbyConfig.getConnection();
        System.out.println(connection);
        try {
            Statement statement = connection.createStatement();
            statement.executeQuery("");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
