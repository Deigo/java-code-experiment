package com.mars.last.service;

import com.mars.last.dao.files.UserFile;
import com.mars.last.pojo.User;

public class UserService {
    private UserFile userFile;

    public UserService() {
        this.userFile = new UserFile();
    }

    public void saveUser(User user) {
        this.userFile.saveUser(user);
    }
}
