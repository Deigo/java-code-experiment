package com.mars.last.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DerbyConfig {
    private String url = "jdbc:mysql://localhost:3306/last_experim";
    private String username = "root";
    private String password = "123456";

    public Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection("jdbc:derby://127.0.0.1:1527//山东工商学院;create=true;");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
