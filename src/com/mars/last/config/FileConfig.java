package com.mars.last.config;

import java.io.*;
import java.util.ResourceBundle;

public class FileConfig {

    private String FILE_LOCATION = "C:\\last";

    public FileConfig() {

    }

    public File GetUserFile() {
        File userFile = new File(this.FILE_LOCATION+"\\User.txt");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(userFile), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return userFile;
    }

    public BufferedWriter getUserBufferedWriter() {
        File userFile = new File(this.FILE_LOCATION+"\\User.txt");
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(userFile), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return bufferedWriter;
    }
}
