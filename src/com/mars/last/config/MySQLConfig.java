package com.mars.last.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;




public class MySQLConfig {

    private String url = "jdbc:mysql://localhost:3306/last_experim";
    private String username = "root";
    private String password = "123456";

    public Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
