package com.mars.last.dao.sql;

import java.sql.*;

/**
 * 操作derby数据库
 */
public class TestDerby {
    public static void main(String[] args) {
        Connection con = null;
        Statement sta = null;
        String SQL = null;
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
//            Class.forName("org.apache.derby.jdbc.ClientDriver");
            con = DriverManager.getConnection("jdbc:derby:D:/MYDB;create=true;user=root;password=123");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            sta = con.createStatement();
            SQL = "create table biao"+
            "(number char(10) primary key,name varchar(40), score float)";
            sta.executeUpdate(SQL);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.out.println("该表已存在，不在重新创建");

        }

        try {
            SQL = "insert into biao values('B001','张小三',90.8),('B002','李四',88.87)";
            sta.executeUpdate(SQL);
            SQL = "update biao set score =92,name = '李小四' where number = 'B002'";
            sta.executeUpdate(SQL);
            ResultSet rs = sta.executeQuery("SELECT * FROM biao");
            while (rs.next()){
                String number = rs.getString(1);
                System.out.println(number+"\t");
                String name = rs.getString(2);
                System.out.println(name+"\t");
                float score = rs.getFloat(3);
                System.out.println(score);
            }
            con.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
