package com.mars.last.dao.files;

import com.mars.last.config.FileConfig;
import com.mars.last.pojo.User;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * User对象操作
 */
public class UserFile {
    private  FileConfig fileConfig;
    private BufferedWriter bufferedWriter;

    public UserFile() {
        this.fileConfig = new FileConfig();
    }

    public void saveUser(User user) {
        this.bufferedWriter = this.fileConfig.getUserBufferedWriter();
        String userInfo = "";
        try {
            userInfo = String.valueOf(user.getId())+" "+new String(user.getName().getBytes("GBK"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            this.bufferedWriter.write(userInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                this.bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
