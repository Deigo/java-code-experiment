package com.mars.last.frame;

import javax.swing.*;
import java.awt.*;


public class FileFrame extends JFrame {
    private JLabel title;
    private JPanel bPanel;
    private JButton toFiles;
    private JButton toSQL;

    public FileFrame() throws HeadlessException {
        this.title = new JLabel("文件存储",JLabel.CENTER);
        this.title.setFont(new Font("",Font.BOLD,25));
        this.bPanel = new JPanel();
        this.toFiles = new JButton("文件存储");
        this.toSQL = new JButton("SQL存储");
        this.bPanel.add(toFiles);
        this.bPanel.add(toSQL);
        add(this.title,BorderLayout.NORTH);
        add(this.bPanel,BorderLayout.CENTER);
        setBounds(100,100,400,200);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序
    }
}
