package com.mars.comment;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class MyFrame extends JFrame   //构建界面
{

    private static final long serialVersionUID = 1L;
    MainClass_2 main_class = new MainClass_2();
    find fi = new find();
    findall f2 = new findall();
    Scanner reader = new Scanner(System.in);
    JLabel label = new JLabel("序号");
    JLabel label2 = new JLabel("姓名");
    JLabel label3 = new JLabel("性别");
    JLabel label4 = new JLabel("生日");
    JLabel label5 = new JLabel("地址");
    JLabel label6 = new JLabel("薪水");


    // 注意：构造参数，16表示16列，用于计算宽度显示，并不是字符个数限制
    JTextField textField = new JTextField(35);
    JTextField textField2 = new JTextField(35);
    JTextField textField3 = new JTextField(35);
    JTextField textField4 = new JTextField(35);
    JTextField textField5 = new JTextField(35);
    JTextField textField6 = new JTextField(35);
    JButton button3 = new JButton("显示所有");
    JButton button = new JButton("确定");
    JButton button2 = new JButton("按姓名查询");
    public MyFrame(String title)
    {
        super(title);//调用父类JFrame的构造方法

        Container contentPane = getContentPane();
        contentPane.setLayout(new FlowLayout());

        // 添加控件
        contentPane.add(label);
        contentPane.add(textField);
        contentPane.add(label2);
        contentPane.add(textField2);
        contentPane.add(label3);
        contentPane.add(textField3);
        contentPane.add(label4);
        contentPane.add(textField4);
        contentPane.add(label5);
        contentPane.add(textField5);
        contentPane.add(label6);
        contentPane.add(textField6);
        contentPane.add(button);
        contentPane.add(button2);
        contentPane.add(button3);

        // 按钮点击处理
        button.addActionListener(e->onButtonOk());
        button2.addActionListener(e->onButtonOk2());
        button3.addActionListener(e->onButtonOk3());
    }

    // 事件处理
    private void onButtonOk()
    {
        // 消息提示框 (注意 showMessageDialog() 是静态方法 )
        JOptionPane.showMessageDialog(this, "输入了: " + textField.getText()+" "+textField2.getText()+" "+textField3.getText()+" "+textField4.getText()+" "+textField5.getText()+" "+textField6.getText());
        String text = textField.getText();
        String text2 = textField2.getText();
        String text3 = textField3.getText();
        String text4 = textField4.getText();
        String text5 = textField5.getText();
        String text6 = textField6.getText();
        float ans7 = Float.valueOf(text6).floatValue();

        main_class.setw(text,text2,text3,text4,text5,ans7);

    }
    private void onButtonOk2()// 按照姓名查找
    {
        String text2 = textField2.getText();
        int flag=fi.search(text2);
        if(flag==0)
            JOptionPane.showMessageDialog(this, "未查询到" );
        else{
            String name= fi.findname();
            String id = fi.findid();
            String sex= fi.findsex();
            String birth = fi.findbirth();
            String dress = fi.finddress();
            float mo = fi.findmo();
            JOptionPane.showMessageDialog(this, "查询到"+id+" "+name+" "+sex+" "+birth+" "+dress+" "+String.valueOf(mo) );
        }

    }
    private void onButtonOk3()
    {
        String name5[]=new String[100];
        String id5[]=new String[100];
        String dress5[]=new String[100];
        String birth5[]=new String[100];
        String sex5[]=new String[100];
        float mo5[]=new float[100];
        int count=f2.search();
        int i=1;
        for(i=1;i<=count;i++)
        {
            name5[i]=f2.name2[i];
            id5[i]=f2.id2[i];
            dress5[i]=f2.dress2[i];
            sex5[i]=f2.sex2[i];
            mo5[i] = f2.mo2[i];
            birth5[i]=f2.birth2[i];
            JOptionPane.showMessageDialog(this, "查询到"+id5[i]+" "+name5[i]+" "+sex5[i]+" "+birth5[i]+" "+dress5[i]+" "+String.valueOf(mo5[i]) );
        }

    }
}