package com.mars.comment;

import javax.swing.JFrame;
public class text   //构建窗口
{
    public static void main(String[] args)
    {
        // JFrame指一个窗口，构造方法的参数为窗口标题
        MyFrame frame = new MyFrame("职工管理");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // 设置窗口的其他参数，如窗口大小
        frame.setSize(400, 500);

        // 显示窗口
        frame.setVisible(true);
    }
}