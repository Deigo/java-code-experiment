package com.mars.day1.dao;


import com.mars.day1.config.DerbyConfig;
import com.mars.day1.pojo.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentMapper {
    private Connection connection;
    private Statement statement;
    public StudentMapper() {
        DerbyConfig derbyConfig = new DerbyConfig();
        this.connection = derbyConfig.getConnection();
        try {
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void createTable(){
        String sql = "create TABLE 计科19 (number int primary key, name varchar(20), sex char(6),age double)";
        try {
            this.updateSQL(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addStudent(Student student){
        String sql1 ="insert into 计科19(number,name,sex,age) VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(1,student.getNumber());
//            pst.setString(2,new String(student.getName().getBytes("GBK")));
            pst.setString(2,student.getName());
//            pst.setString(3,new String(student.getSex().getBytes("GBK")));
            pst.setString(3,student.getSex());
            pst.setDouble(4,student.getAge());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Student queryStudentByNum(Long number){
        Student student = null;
        String sql1 ="select * from 计科19 where number = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(1,number);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
                student = new Student();
                student.setNumber(resultSet.getLong("number"));
                student.setName(resultSet.getString("name"));
                student.setSex(resultSet.getString("sex"));
                student.setAge(resultSet.getDouble("age"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }
    public List<Student> queryStudentByAge(Double age){
        ArrayList<Student> stus = new ArrayList<>();
        String sql1 ="select * from 计科19 where age = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setDouble(1,age);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
                Student student = new Student();
                student.setNumber(resultSet.getLong("number"));
                student.setName(resultSet.getString("name"));
                student.setSex(resultSet.getString("sex"));
                student.setAge(resultSet.getDouble("age"));
                stus.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stus;
    }
    public void deleteStudentByNum(Long number){
        String sql = "DELETE from 计科19 WHERE number = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql);
            pst.setLong(1,number);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Student> selectAll(){
        ArrayList<Student> stus = new ArrayList<>();
        String sql = "select  * from 计科19";
        try {
            ResultSet resultSet = this.querySQL(sql);
            while (resultSet.next()){
                Student student = new Student();
                student.setNumber(Long.valueOf(resultSet.getString("number")));
                student.setName(resultSet.getString("name"));
                student.setSex(resultSet.getString("sex"));
                student.setAge(Double.valueOf(resultSet.getString("age")));
                stus.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stus;
    }

    public void updateSQL(String sql) throws SQLException {
        this.statement.executeUpdate(sql);
    }

    public ResultSet querySQL(String sql) throws SQLException {
        return this.statement.executeQuery(sql);
    }

}
