package com.mars.day1;

import com.mars.day1.dao.StudentMapper;
import com.mars.day1.pojo.Student;

import java.util.List;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        StudentMapper studentMapper = new StudentMapper();
        System.out.println("数据库已连接");
//        studentMapper.createTable();   //创建数据库，已存在则忽略
        Scanner scanner = new Scanner(System.in);
        System.out.println("开始添加学生");

        while (true){
            System.out.print("请输入学生姓名:");
            String name = scanner.next();
            System.out.print("请输入学生性别:");
            String sex = scanner.next();
            System.out.print("请输入学生年龄:");
            Double age = scanner.nextDouble();
            Student student = new Student();
            List<Student> students = studentMapper.selectAll();
            student.setNumber(Long.valueOf(students.size()+1));
            student.setName(name);
            student.setSex(sex);
            student.setAge(age);
            studentMapper.addStudent(student);
            System.out.println("数据添加成功");
            System.out.println("输入1继续，非1结束0");
            int order = scanner.nextInt();

            if(order != 1)
                break;
        }
        System.out.println("添加结束");

        List<Student> studentsAfter = studentMapper.selectAll();
        System.out.println("数据插入后");
        System.out.println(studentsAfter);

    }
}
