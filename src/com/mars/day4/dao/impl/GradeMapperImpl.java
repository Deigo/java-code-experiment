package com.mars.day4.dao.impl;


import com.mars.day4.config.DerbyConfig;
import com.mars.day4.dao.IGradeMapper;
import com.mars.day4.pojo.Grade;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Grade 数据库实现
 */
public class GradeMapperImpl implements IGradeMapper {

    private final String TABLE_NAME = "grade"; //表名grade
    private Connection connection;
    private Statement statement;
    public GradeMapperImpl() {
        DerbyConfig derbyConfig = new DerbyConfig();
        this.connection = derbyConfig.getConnection();
        try {
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean createTable(){
        String sql = "create TABLE " +
                TABLE_NAME +
                "(gid int PRIMARY KEY,uid varchar(20),end_grade double,finish_time DATE)";
        try {
            this.updateSQL(sql);
            return true;
        } catch (SQLException e) {
            System.out.println("grade数据库库已创建,创建取消");
//            e.printStackTrace();
        }
        return false;
    }

    public void addGrade(Grade grade){
        String sql1 ="insert into " +
                TABLE_NAME +
                "(gid ,uid ,end_grade,finish_time) VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(1, grade.getGid());
            pst.setString(2, grade.getUid());
            pst.setDouble(3,grade.getEndGrade());
            pst.setDate(4, new Date(grade.getFinishTime().getTime()));
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Grade queryGradeById(Long gid){
        Grade grade = null;
        String sql1 ="select * from " + TABLE_NAME +" where gid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(1,gid);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
                grade = new Grade();
                grade.setGid(resultSet.getLong("gid"));
                grade.setUid(resultSet.getString("uid"));
                grade.setEndGrade(resultSet.getDouble("end_grade"));
                grade.setFinishTime(new java.util.Date(resultSet.getDate("finish_time").getTime()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return grade;
    }

    public void updateGrade(Grade grade){
        String sql1 ="update " + TABLE_NAME + " set uid = ?,end_grade= ?,finish_time=? where gid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(4, grade.getGid());
            pst.setString(1, grade.getUid());
            pst.setDouble(2,grade.getEndGrade());
            pst.setDate(3, new Date(grade.getFinishTime().getTime()));
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void deleteGradeById(Long gid){
        String sql = "DELETE from " + TABLE_NAME + " WHERE gid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql);
            pst.setLong(1,gid);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Grade> selectAll(){
        List<Grade> grades = new ArrayList<>();
        String sql = "select  * from " + TABLE_NAME;
        try {
            ResultSet resultSet = this.querySQL(sql);
            while (resultSet.next()){
                Grade grade = new Grade();
                grade.setGid(resultSet.getLong("gid"));
                grade.setUid(resultSet.getString("uid"));
                grade.setEndGrade(resultSet.getDouble("end_grade"));
                grade.setFinishTime(new java.util.Date(resultSet.getDate("finish_time").getTime()));
                grades.add(grade);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return grades;
    }

    public void updateSQL(String sql) throws SQLException {
        this.statement.executeUpdate(sql);
    }

    public ResultSet querySQL(String sql) throws SQLException {
        return this.statement.executeQuery(sql);
    }

}
