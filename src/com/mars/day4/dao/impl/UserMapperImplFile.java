package com.mars.day4.dao.impl;

import com.mars.day4.dao.IUserMapper;
import com.mars.day4.pojo.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户表 文件操作
 */
public class UserMapperImplFile implements IUserMapper {

    private final String FILE_PATH = "c:/last_experim/user.txt";

    public UserMapperImplFile() {
        File file = new File(FILE_PATH);
    }

    @Override
    public boolean createTable() {
        return false;
    }


    @Override
    public void addUser(User user) {
        try {
            File file = new File(FILE_PATH);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true), "UTF-8"));
            String str = "";
            str = user.getUid()+" "+user.getPasswd();
            bufferedWriter.newLine();
            bufferedWriter.write(str);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User queryUserById(String uId) {
        List<User> users = selectAll();
        for (User user : users) {
            if (user.getUid().equals(uId))
                return user;
        }
        return null;
    }

    @Override
    public void updateUser(User user) {
        List<User> users = selectAll();
        for (User u : users) {
            if (u.getUid().equals(user.getUid()))
                u.setPasswd(user.getPasswd());
        }
        updateFileByList(users);
    }

    @Override
    public void deleteUserById(String uid) {
        List<User> users = selectAll();
        User deleteUser = new User();
        for (User u : users) {
            if (u.getUid().equals(uid))
                deleteUser = u;
        }
        users.remove(deleteUser);
        updateFileByList(users);
    }

    @Override
    public List<User> selectAll() {
        List<User> users = new ArrayList<>();
        BufferedReader bufferedReader = null;
        try {
            File file = new File(FILE_PATH);
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String str = "";
            while ((str=bufferedReader.readLine())!=null){
                User user = new User();
                String[] strs = str.split(" ");
                int count = 1;
                for (String s : strs) {
                    if (count == 1)
                        user.setUid(s);
                    if (count == 2){
                        user.setPasswd(s);
                    }
                    count++;
                }
                users.add(user);
            }
            bufferedReader.close();
            return  users;
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

        }
        return null;
    }
    public void updateFileByList(List<User> users){
        try {
            File file = new File(FILE_PATH);

            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            String str = "";
            int count = 0;
            for (User user : users) {
                str = user.getUid()+" "+user.getPasswd();
                if (count != 0)
                    bufferedWriter.newLine();
                count++;
                bufferedWriter.write(str);
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
