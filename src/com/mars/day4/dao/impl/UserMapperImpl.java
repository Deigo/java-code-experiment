package com.mars.day4.dao.impl;


import com.mars.day4.config.DerbyConfig;
import com.mars.day4.dao.IUserMapper;
import com.mars.day4.pojo.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * UserE 数据库实现
 */
public class UserMapperImpl implements IUserMapper {
    private Connection connection;
    private Statement statement;
    public UserMapperImpl() {
        DerbyConfig derbyConfig = new DerbyConfig();
        this.connection = derbyConfig.getConnection();
        try {
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean createTable(){
        String sql = "create table userE(uid varchar(20) PRIMARY KEY,passwd varchar(20))";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            System.out.println("userE数据库表已创建,创建取消");
            return false;
        }
    }

    public void addUser(User user){
        String sql1 ="insert into userE(uid,passwd) VALUES(?,?)";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(1, user.getUid());
            pst.setString(2, user.getPasswd());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User queryUserById(String uId){
        User user = null;
        String sql1 ="select * from userE where uId = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(1,uId);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
               user = new User();
               user.setUid(resultSet.getString("uid"));
               user.setPasswd(resultSet.getString("passwd"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    public void updateUser(User user){
        String sql1 ="update userE set passwd= ? where id = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(2,user.getUid());
            pst.setString(1,user.getPasswd());
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void deleteUserById(String uid){
        String sql = "DELETE from userE WHERE uid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql);
            pst.setString(1,uid);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> selectAll(){
        ArrayList<User> users = new ArrayList<>();
        String sql = "select  * from userE";
        try {
            ResultSet resultSet = this.querySQL(sql);
            while (resultSet.next()){
                User user = new User();
                user.setUid(resultSet.getString("uid"));
                user.setPasswd(resultSet.getString("passwd"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public void updateSQL(String sql)  {
        try {
            this.statement.executeUpdate(sql);
        } catch (SQLException throwables) {
            System.out.println("Question数据库表已创建,创建取消");
        }
    }

    public ResultSet querySQL(String sql)  {
        try {
            return this.statement.executeQuery(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
