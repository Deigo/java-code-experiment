package com.mars.day4.dao.impl;


import com.mars.day4.config.DerbyConfig;
import com.mars.day4.dao.IQuestionMapper;
import com.mars.day4.pojo.Question;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Question 数据库实现
 */
public class QuestionMapperImpl implements IQuestionMapper {
    private Connection connection;
    private Statement statement;
    public QuestionMapperImpl() {
        DerbyConfig derbyConfig = new DerbyConfig();
        this.connection = derbyConfig.getConnection();
        try {
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean createTable(){
        String sql = "create table question(qid int PRIMARY KEY,question_str VARCHAR(255),answer int, score DOUBLE)";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            System.out.println("Question数据库表已创建,创建取消");
            return false;
        }
    }

    public void addQuestion(Question question){
        String sql1 ="insert into question(qid ,question_str ,answer , score) VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(1, question.getQid());
            pst.setString(2, question.getQuestionStr());
            pst.setInt(3, question.getAnswer());
            pst.setDouble(4,question.getScore());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Question queryQuestionById(Long qid){
        Question question = null;
        String sql1 ="select * from question where qid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(1,String.valueOf(qid));
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
                question = new Question();
                question.setQid(resultSet.getLong("qid"));
                question.setQuestionStr(resultSet.getString("question_str"));
                question.setAnswer(resultSet.getInt("answer"));
                question.setScore(resultSet.getDouble("score"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return question;
    }

    public void updateQuestion(Question question){
        String sql1 ="update question set question_str=? ,answer=? , score=? where qid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setLong(4, question.getQid());
            pst.setString(1, question.getQuestionStr());
            pst.setInt(2, question.getAnswer());
            pst.setDouble(3,question.getScore());
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void deleteQuestionById(Long qid){
        String sql = "DELETE from question WHERE qid = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql);
            pst.setString(1,String.valueOf(qid));
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Question> selectAll(){
        ArrayList<Question> questions = new ArrayList<>();
        String sql = "select  * from question";
        try {
            ResultSet resultSet = this.querySQL(sql);
            while (resultSet.next()){
                Question question = new Question();
                question.setQid(resultSet.getLong("qid"));
                question.setQuestionStr(resultSet.getString("question_str"));
                question.setAnswer(resultSet.getInt("answer"));
                question.setScore(resultSet.getDouble("score"));
                questions.add(question);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return questions;
    }

    public void updateSQL(String sql)  {
        try {
            this.statement.executeUpdate(sql);
        } catch (SQLException throwables) {
//            throwables.printStackTrace();
            System.out.println("Question数据库表已创建,创建取消");
        }
    }

    public ResultSet querySQL(String sql) {
        try {
            return this.statement.executeQuery(sql);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

}
