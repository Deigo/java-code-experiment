package com.mars.day4.dao;

import com.mars.day4.pojo.User;

import java.util.List;

/**
 * 考生表user
 */
public interface IUserMapper {
    /**
     * 创建考生表
     * @return
     */
    public boolean createTable();

    /**
     * 添加考生
     * @param user
     */
    public void addUser(User user);

    /**
     * 根据考生id查询考生
     * @param uId
     * @return
     */
    public User queryUserById(String uId);

    /**
     * 更新考生
     * @param user
     */
    public void updateUser(User user);

    /**
     * 删除考生
     * @param uid
     */
    public void deleteUserById(String uid);

    /**
     * 查询所有考生
     * @return
     */
    public List<User> selectAll();

}
