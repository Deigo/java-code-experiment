package com.mars.day4.dao;

import com.mars.day4.pojo.Question;

import java.util.List;

/**
 * 题目表question
 */
public interface IQuestionMapper {

    /**
     * 创建题目表question
     * @return
     */
    public boolean createTable();

    /**
     * 添加题目
     * @param question
     */
    public void addQuestion(Question question);

    /**
     * 根据id查询题目
     * @param qid
     * @return
     */
    public Question queryQuestionById(Long qid);


    /**
     * 更新题目
     * @param question
     */
    public void updateQuestion(Question question);

    /**
     * 删除题目
     * @param qid
     */
    public void deleteQuestionById(Long qid);

    /**
     * 查询所有题目
     * @return
     */
    public List<Question> selectAll();

}
