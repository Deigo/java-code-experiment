package com.mars.day4.dao;


import com.mars.day4.pojo.Grade;
import java.util.List;

/**
 * grade成绩表操作
 */
public interface IGradeMapper {

    /**
     * 在数据库中创建grade表
     * @return
     */
    public boolean createTable();

    /**
     * 添加成绩
     * @param grade
     */
    public void addGrade(Grade grade);

    /**
     * 根据id查询成绩
     * @param gid
     * @return
     */
    public Grade queryGradeById(Long gid);

    /**
     * 更新成绩
     * @param grade
     */
    public void updateGrade(Grade grade);


    /**
     * 删除成绩
     * @param gid
     */
    public void deleteGradeById(Long gid);

    /**
     * 查询所有成绩
     * @return
     */
    public List<Grade> selectAll();

}
