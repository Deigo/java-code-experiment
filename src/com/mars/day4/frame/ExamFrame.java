package com.mars.day4.frame;

import com.mars.day4.dao.IGradeMapper;
import com.mars.day4.dao.IQuestionMapper;
import com.mars.day4.dao.impl.GradeMapperImpl;
import com.mars.day4.dao.impl.QuestionMapperImpl;
import com.mars.day4.pojo.Grade;
import com.mars.day4.pojo.Question;
import com.mars.day4.pojo.User;
import com.mars.day4.utils.Counter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 考试页面
 */
public class ExamFrame extends JFrame {
    //考生信息
    private User user;
    private Font font;
    private Double endGrade;

    //剩余时间
    private JPanel timeP;
    private JLabel timeTipL;
    private JLabel timeL;
    private final String MINUTES = "90"; //考试时间
    private Counter counter;



    //题目
    private JPanel dataP;
    private List<JPanel> questionsP;
    private List<Question> questions;
    private List<Question> answers;              //做答结果


    //提交
    private JPanel submitP;
    private JButton submitBtn;

    //question grade持久层数据
    private IQuestionMapper questionMapper;
    private IGradeMapper gradeMapper;



    public ExamFrame(User user) {
        this.user = user;

        //持久层操作对象初始化
        this.questionMapper = new QuestionMapperImpl();
        this.gradeMapper = new GradeMapperImpl();

        //字体
        font = new Font("宋体",Font.BOLD,15);

        //倒计时框
        timeP = new JPanel();
        timeTipL = new JLabel("离考试结束还有:");
        timeL = new JLabel("显示时间");
        timeTipL.setFont(new Font("黑体",Font.BOLD,25));
        timeL.setFont(new Font("黑体",Font.BOLD,25));
        timeP.add(timeTipL,BorderLayout.WEST);
        timeP.add(timeL,BorderLayout.EAST);

        //题目
        dataP = new JPanel();
        questionsP = new ArrayList<>();
        questions = this.questionMapper.selectAll();
        dataP.setSize( new Dimension());
        updateData();

        //提交按钮
        submitP = new JPanel();
        submitBtn = new JButton("提交");
        submitP.add(submitBtn);
        submitBtn.setFont(new Font("黑体",Font.BOLD,25));
        setSubmitBtn(user);

        //添加至画板
        add(timeP, BorderLayout.NORTH);
        add(dataP,BorderLayout.CENTER);
        add(submitP,BorderLayout.SOUTH);

        //开启计时器
        counter = new Counter(timeL,this);
        counter.startTime(MINUTES);

        //画板初始化设置
        setTitle("考试系统");
        setSize(800,questions.size()*80+80);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序
    }

    private void setSubmitBtn(User user) {
        this.submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                submitPaper(user);
                counter.stopTimer();
            }
        });
        addWindowListener(new WindowListener() {
            public void windowOpened(WindowEvent e) {}
            public void windowClosing(WindowEvent e) {}
            public void windowClosed(WindowEvent e) {
                System.out.println("考生退出");
                stopCounter();
            }
            public void windowIconified(WindowEvent e) {}
            public void windowDeiconified(WindowEvent e) {}
            public void windowActivated(WindowEvent e) {}
            public void windowDeactivated(WindowEvent e) {}
        });
    }

    private void stopCounter() {
        this.counter.stopTimer();
    }


    private void submitPaper(User user) {
        answers = new ArrayList<>();
        //获取所有选项
        for (JPanel panel : questionsP) {
//            int count = panel.getComponentCount();`
            int num = 0;
            Question question = new Question();
            for (Component c : panel.getComponents()) {

                if (c instanceof JTextField){
                    String str = ((JTextField)c).getText();
                    question.setQid(Long.valueOf(str));
//                            System.out.println(str);
                }

                if(c instanceof JRadioButton){
                    if(((JRadioButton)c).isSelected()){
                        String str = ((JRadioButton)c).getText();
//                        System.out.println(str);
                        if ("对".equals(str)){
//                            System.out.println("选择对");
                            question.setAnswer(1);
                            num++;
                        } else if ("错".equals(str)){
//                            System.out.println("选择错");
                            question.setAnswer(0);
                            num++;
                        }

                    }
                }

            }
            if (num == 0){
//                System.out.println("未选择");
                question.setAnswer(2);
            }

            answers.add(question);
        }
        //计算得分
        endGrade = caculateGrade();
        //存入数据库
        Grade grade = new Grade();
        List<Grade> grades = gradeMapper.selectAll();
        grade.setGid(Long.valueOf(grades.size()+1));
        grade.setUid(user.getUid());
        grade.setEndGrade(endGrade);
        grade.setFinishTime(new Date());
        gradeMapper.addGrade(grade);
        JOptionPane.showMessageDialog(null, "您的成绩为："+endGrade, "考试结束", JOptionPane.ERROR_MESSAGE);
        closeThis();
    }

    private Double caculateGrade() {
        double grade = 0.0;
        for (Question answer : this.answers) {
            Question question = this.questionMapper.queryQuestionById(answer.getQid());
            Double score = question.getScore();
            int realAnswer = question.getAnswer();
            if (answer.getAnswer() == realAnswer)
                grade += score;

        }
        return grade;
    }

    private void updateData(){
        this.questions = this.questionMapper.selectAll();
        int count = 1;
        for (Question question : this.questions) {
            JPanel panel = new JPanel();
            JTextField qidTextField = new JTextField(String.valueOf(question.getQid()));
            qidTextField.setVisible(false); //题目id,设置为不可见

            JLabel jLabel = new JLabel(String.valueOf(count)+"."+question.getQuestionStr());
            jLabel.setFont(font);
            panel.add(qidTextField);
            panel.add(jLabel);
            JRadioButton c1= new JRadioButton("对");
            JRadioButton c2 = new JRadioButton("错");
            ButtonGroup bg = new ButtonGroup();

            bg.add(c1);
            bg.add(c2);
            panel.add(c1);
            panel.add(c2);
            count++;
            this.questionsP.add(panel);
        }
        for (JPanel panel : this.questionsP) {
            this.dataP.add(panel);
        }
        this.dataP.setLayout(new GridLayout(this.questions.size()+1,1));
        this.repaint();
    }

    public void closeThis(){
        dispose();
    }
}
