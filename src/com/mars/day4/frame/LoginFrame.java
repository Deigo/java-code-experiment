package com.mars.day4.frame;

import com.mars.day4.dao.IUserMapper;
import com.mars.day4.dao.impl.UserMapperImpl;
import com.mars.day4.pojo.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ������¼
 */
public class LoginFrame extends JFrame {


    //�˺�����
    private JPanel jPanel;
    private JPanel accountP;
    private JLabel accountL;
    private JTextField accountT;
    private JLabel passwdL;
    private JPanel passwdP;
    private JPasswordField passwdT;
    private JPanel submitP;
    private JButton submitBtn;
    private Font font;

    public LoginFrame() throws HeadlessException {
        setTitle("������¼");
        setSize(400,200);

        jPanel = new JPanel();
        font = new Font("����", Font.BOLD, 20);

        //�˺�
        accountP = new JPanel();
        accountL = new JLabel("�˺ţ�");
        accountT = new JTextField(15);
        accountL.setFont(font);
        accountP.setLayout(new FlowLayout());
        accountP.add(accountL);
        accountP.add(accountT);

        //����
        passwdP = new JPanel();
        passwdL = new JLabel("���룺");
        passwdT = new JPasswordField(15);
        passwdL.setFont(font);
        passwdP.setLayout(new FlowLayout());
        passwdP.add(passwdL);
        passwdP.add(passwdT);

        //��¼
        submitP  = new JPanel();
        submitBtn = new JButton("��¼");
        submitBtn.setFont(font);
        submitP.add(submitBtn);

        jPanel.add(accountP,BorderLayout.CENTER);
        jPanel.add(passwdP,BorderLayout.NORTH);
        add(jPanel,BorderLayout.CENTER);
        add(submitP,BorderLayout.SOUTH);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //�����û��ڴ˿�����������رա�ʱĬ��ִ�еĲ�����ʹ��System exit�����˳�exit����
        submitBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String accountTText = accountT.getText();
                String passwdTText = passwdT.getText();
                User user = new User();
                user.setUid(accountTText);
                user.setPasswd(passwdTText);
                if (userIsExit(user)){
                    closeThis();
                    new ExamFrame(user);
                }
                System.out.println(accountTText);
                System.out.println(passwdTText);

            }
        });

    }

    private boolean userIsExit(User user) {
        IUserMapper userMapper = new UserMapperImpl();
        User userE = userMapper.queryUserById(user.getUid());
        if (userE == null){
            JOptionPane.showMessageDialog(null, "�û�������", "", JOptionPane.ERROR_MESSAGE);
            return false;
        }else if (!userE.getPasswd().equals(user.getPasswd())){
            JOptionPane.showMessageDialog(null, "�������", "", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        return true;
    }

    private void closeThis(){
        dispose();
    }
}
