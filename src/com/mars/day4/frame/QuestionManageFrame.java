package com.mars.day4.frame;

import com.mars.day4.dao.IQuestionMapper;
import com.mars.day4.dao.impl.QuestionMapperImpl;
import com.mars.day4.pojo.Question;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;


/**
 * 题目表管理页面
 */
public class QuestionManageFrame extends JFrame {

    //菜单栏

    //创建并添加菜单栏
    JMenuBar menuBar ;
    JButton userBtn;
    JButton questionBtn;
    JButton gradeBtn;


    //数据显示表格
    private JPanel dataP;
    private  JTable dataT;
    private Vector data;
    private DefaultTableModel model;
    private IQuestionMapper questionMapper;

    //操作提示栏
    private JPanel opPanel;
    private JLabel opLable;
    private JTextField opTextField;

    //操作按钮
    private JPanel pBtn;
    private JButton addBtn;
    private JButton deleteBtn;
    private JButton queryBtn;
    private JButton updateBtn;
    private JButton updateDataBtn;


    public QuestionManageFrame() throws HeadlessException {

        setTitle("考试后台管理系统");

        //初始化持久层操作对象
        this.questionMapper = new QuestionMapperImpl();

        //初始化菜单栏
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        userBtn = new JButton("用户表");
        questionBtn = new JButton("题目表");
        gradeBtn = new JButton("成绩表");
        menuBar.add(userBtn);
        menuBar.add(questionBtn);
        menuBar.add(gradeBtn);
        questionBtn.setBackground(new Color(0x8D96EA));

        dataP= new JPanel();
        initTable();
        add(dataP,BorderLayout.NORTH);

        opPanel = new JPanel();
        opLable = new JLabel();
        opTextField = new JTextField(40);
        opTextField.setEditable(false);
        opPanel.setLayout(new FlowLayout());
        opLable.setText("操作信息：");
        opPanel.add(opLable);
        opPanel.add(opTextField);
        add(opPanel,BorderLayout.CENTER);

        pBtn = new JPanel();
        addBtn = new JButton("添加题目");
        deleteBtn = new JButton("删除题目");
        queryBtn = new JButton("查询题目");
        updateBtn = new JButton("更新题目");
        updateDataBtn = new JButton("刷新");

        pBtn.add(addBtn);
        pBtn.add(deleteBtn);
        pBtn.add(queryBtn);
        pBtn.add(updateBtn);
        pBtn.add(updateDataBtn);
        add(pBtn,BorderLayout.SOUTH);
        opTextField.setText("数据加载成功");
        setBtnsListener();

        setSize(800,600);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序

    }

    private void initTable() {
        model = new DefaultTableModel();

        Vector<String> name = new Vector<>();
        name.add("题目Id");
        name.add("题目内容");
        name.add("题目答案(0错，1对)");
        name.add("题目分值");

        data = new Vector();
        model.setDataVector(data,name);
        dataT = new JTable(model);

        updateData();
        dataP.add(new JScrollPane(dataT));

    }

    private void setBtnsListener() {
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                JTextField questionInput = new JTextField();
                JTextField answerInput = new JTextField();
                JTextField scoreInput = new JTextField();

                //题目Id,题目内容,题目答案,题目分值;
                Object[] message1 = {
                        "题目Id:",idInput,
                        "题目内容",questionInput,
                        "题目答案(0错,1对)",answerInput,
                        "题目分值",scoreInput,
                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Question question = new Question();
                    question.setQid(Long.valueOf(idInput.getText()));
                    question.setQuestionStr(questionInput.getText());
                    question.setAnswer(Integer.parseInt(answerInput.getText()));
                    question.setScore(Double.valueOf(scoreInput.getText()));
                    questionMapper.addQuestion(question);
                    opTextField.setText("添加成功");
                    updateData();
                }
            }
        });

        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                //题目Id,题目内容,题目答案,题目分值;
                Object[] message1 = {
                        "id:",idInput,

                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    questionMapper.deleteQuestionById(Long.valueOf(idInput.getText()));
                    updateData();
                    opTextField.setText("删除成功");
                }
            }
        });

        updateBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                JTextField questionInput = new JTextField();
                JTextField answerInput = new JTextField();
                JTextField scoreInput = new JTextField();

                //题目Id,题目内容,题目答案,题目分值;
                Object[] message1 = {
                        "题目Id:",idInput,
                        "题目内容",questionInput,
                        "题目答案",answerInput,
                        "题目分值",scoreInput,
                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Question question = new Question();
                    question.setQid(Long.valueOf(idInput.getText()));
                    question.setQuestionStr(questionInput.getText());
                    question.setAnswer(Integer.parseInt(answerInput.getText()));
                    question.setScore(Double.valueOf(scoreInput.getText()));
                    questionMapper.updateQuestion(question);
                    opTextField.setText("更新成功");
                    updateData();
                }
            }
        });
        queryBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                //"id","姓名","性别","出生","地址","工资"}
                Object[] message1 = {
                        "输入查询id:",idInput,

                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Question result = questionMapper.queryQuestionById(Long.valueOf(idInput.getText()));
                    if (result != null){
                        String str = "查询成功："+result.toString();
                        JLabel jLabel= new JLabel();
                        jLabel.setText(str);
                       opTextField.setText(str);
                    }

                }
            }
        });
        updateDataBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("数据已更新");
                updateData();
            }
        });
        userBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeThis();
                new UserManageFrame();
            }
        });
        gradeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                closeThis();
                new GradeManageFrame();
            }
        });

    }

    private void updateData() {
        this.data.clear();
        List<Question> questions = this.questionMapper.selectAll();
        for (Question question : questions) {
            Vector<String> row = new Vector<>();
            row.add( String.valueOf(question.getQid()));
            row.add(question.getQuestionStr());
            row.add(String.valueOf(question.getAnswer()));
            row.add(String.valueOf(question.getScore()));
            data.add(row);
        }
        this.repaint();
        this.dataT.updateUI();
    }

    private void closeThis(){
        dispose();
    }

}
