package com.mars.day4;

import com.mars.day4.dao.IQuestionMapper;
import com.mars.day4.dao.IUserMapper;
import com.mars.day4.dao.impl.GradeMapperImpl;
import com.mars.day4.dao.impl.QuestionMapperImpl;
import com.mars.day4.dao.impl.UserMapperImpl;
import com.mars.day4.frame.ExamFrame;
import com.mars.day4.frame.LoginFrame;
import com.mars.day4.pojo.Question;
import com.mars.day4.pojo.User;

import java.util.List;

/**
 * 学生考试   入口
 */
public class MainClass {
    public static void main(String[] args) {
//        new ExamFrame(new User("admin","123456")); //直接进入考试页面
        new LoginFrame(); //进入登录页面
        initTables(); // 初始化数据表
        initTableData();//初始化表中的数据
    }


    private static void initTableData(){
        IQuestionMapper questionMapper = new QuestionMapperImpl();
        IUserMapper userMapper = new UserMapperImpl();
        List<Question> questions = questionMapper.selectAll();
        List<User> users = userMapper.selectAll();
        //验证是否有数据
        if (users.size()==0){
            //加载考生表数据，登录账号：admin，登录密码：123456
            User user = new User();
            user.setUid("admin");
            user.setPasswd("123456");
            userMapper.addUser(user);
        }

        if ( questions.size()==0 ){
            //加载题目数据
            Question q1 = new Question();
            Question q2 = new Question();
            Question q3 = new Question();
            Question q4 = new Question();
            Question q5 = new Question();

            q1.setQid(1l);
            q1.setQuestionStr("荷兰的风车主要用于排水？");
            q1.setAnswer(1);
            q1.setScore(10.0);

            q2.setQid(2l);
            q2.setQuestionStr("鸡尾酒会最早出现于拉美？");
            q2.setAnswer(1);
            q2.setScore(10.0);

            q3.setQid(3l);
            q3.setQuestionStr("武侯祠在四川成都？");
            q3.setAnswer(1);
            q3.setScore(10.0);

            q4.setQid(4l);
            q4.setQuestionStr("公元230年，孙权派卫温率船队到达夷洲。夷洲今是台湾？");
            q4.setAnswer(1);
            q4.setScore(10.0);

            q5.setQid(5l);
            q5.setQuestionStr("以“无字碑”名扬天下的是武则天？");
            q5.setAnswer(1);
            q5.setScore(10.0);


            questionMapper.addQuestion(q1);
            questionMapper.addQuestion(q2);
            questionMapper.addQuestion(q3);
            questionMapper.addQuestion(q4);
            questionMapper.addQuestion(q5);
            System.out.println("初始题目已加载");
        }
    }

    private static void initTables() {
        new UserMapperImpl().createTable();
        new QuestionMapperImpl().createTable();
        new GradeMapperImpl().createTable();
    }
}
