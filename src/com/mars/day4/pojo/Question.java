package com.mars.day4.pojo;

/**
 * 题目
 */
public class Question {
    private Long qid; //题目id
    private String questionStr; //题目内容
    private int answer;  //0为错误 1正确
    private Double score; //题目分值

    public Long getQid() {
        return qid;
    }

    public void setQid(Long qid) {
        this.qid = qid;
    }

    public String getQuestionStr() {
        return questionStr;
    }

    public void setQuestionStr(String questionStr) {
        this.questionStr = questionStr;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Question{" +
                "qid=" + qid +
                ", questionStr='" + questionStr + '\'' +
                ", answer=" + answer +
                ", score=" + score +
                '}';
    }
}
