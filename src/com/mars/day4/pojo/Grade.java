package com.mars.day4.pojo;

import java.util.Date;

/**
 * 分数
 */
public class Grade {
    private Long gid; //分数id
    private String uid; //考生id
    private Double endGrade; //得分
    private Date finishTime;  //完成时间

    public Long getGid() {
        return gid;
    }

    public void setGid(Long gid) {
        this.gid = gid;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Double getEndGrade() {
        return endGrade;
    }

    public void setEndGrade(Double endGrade) {
        this.endGrade = endGrade;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "gid=" + gid +
                ", uid='" + uid + '\'' +
                ", endGrade=" + endGrade +
                ", finishTime=" + finishTime +
                '}';
    }
}
