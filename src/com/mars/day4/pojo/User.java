package com.mars.day4.pojo;

public class User {
    private String uid;  //�˺�
    private String passwd; //����

    public User() {
    }

    public User(String uid, String passwd) {
        this.uid = uid;
        this.passwd = passwd;
    }



    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    @Override
    public String toString() {
        return "User{" +
                "uid='" + uid + '\'' +
                ", passwd='" + passwd + '\'' +
                '}';
    }
}
