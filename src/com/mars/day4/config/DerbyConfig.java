package com.mars.day4.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//import com.mysql.cj.jdbc.Driver;

/**
 * 数据库连接
 */
public class DerbyConfig {

    //Derby连接
    private static final String DRIVER = "org.apache.derby.jdbc.ClientDriver";  //数据库驱动
    private static final String URL = "jdbc:derby://127.0.0.1:1527//last_experim;create=true;";  //数据库连接url

    //MySQL连接
//    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";  //数据库驱动
//    private static final String URL = "jdbc:mysql://127.0.0.1:3306/last_experim";  //数据库url
//    private static final String USERNAME = "root";   //数据库用户名
//    private static final String PASSWORD = "123456";    //密码

    public static Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
//            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            connection = DriverManager.getConnection(URL);
        } catch (SQLException throwables) {
            System.out.println("数据库连接失败，请确保已配置Derby数据库，并开启startNetworkServer");
            throwables.printStackTrace();
        }
        return connection;
    }
}
