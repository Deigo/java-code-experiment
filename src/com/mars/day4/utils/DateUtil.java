package com.mars.day4.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期格式转化
 */
public class DateUtil {
    public static Date StringToDate(String dateStr){
        Date date = null;
        if (dateStr == null || "".equals(dateStr))
            return null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String DateToString(Date date){
        SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd");
        return time.format(date);
    }

    public static Date StringToMinutes(String minutes){
        minutes = String.valueOf(480+Integer.valueOf(minutes));
        Date date = null;
        if (minutes == null || "".equals(minutes))
            return null;
        try {
            Date now = new Date();
            date = new SimpleDateFormat("mm").parse(minutes);
            date = new Date(now.getTime()+date.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
