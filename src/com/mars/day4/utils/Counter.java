package com.mars.day4.utils;

import com.mars.day4.frame.ExamFrame;

import javax.swing.JLabel;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
 
/**
 * 倒计时
 */
public class Counter {

    private JLabel jLabel;
    private ScheduledThreadPoolExecutor scheduled;
    ExamFrame examFrame;


    public Counter(JLabel timeL, ExamFrame examFrame) {
        this.jLabel = timeL;
        this.examFrame = examFrame;
        scheduled = new ScheduledThreadPoolExecutor(2);
    }


    public void startTime(String minutes) {

        Date date = DateUtil.StringToMinutes(minutes);

        scheduled.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                jishi(date);
            }
        }, 0, 1, TimeUnit.SECONDS);

    }
    private void jishi(Date end) {
        long time = (end.getTime() - 1 - System.currentTimeMillis()) / 1000;
        if (time <= 0) {
            stopTimer();
            if (jLabel != null&&this.examFrame!=null){
                jLabel.setText("考试结束");
                this.examFrame.closeThis();
            }

            //提交考试
            return;
        }
        long hour = time / 3600;
        long minute = (time - hour * 3600) / 60;
        long seconds = time - hour * 3600 - minute * 60;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(hour).append("时 ").append(minute).append("分 ").append(seconds).append("秒 ");
        jLabel.setText(stringBuilder.toString());
    }

    /**
     * 停止定时器
     */
    public void stopTimer() {
        if (scheduled != null) {
            scheduled.shutdownNow();
            scheduled = null;
        }
    }

 
}