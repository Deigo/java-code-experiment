package com.mars.day2;

import com.mars.day2.dao.StudentMapper;
import com.mars.day2.pojo.Student;

import java.util.List;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {
        //通过JDBC进行程序设计，输出“山东工商学院”数据库中“计科19”表中的全部内容。
        StudentMapper studentMapper = new StudentMapper();
        List<Student> students = studentMapper.selectAll();
        System.out.println("“山东工商学院”数据库中“计科19”表:");
        students.forEach( student -> {
            System.out.println(student.getNumber()+"|"+student.getName()+"|"+student.getSex()+"|"+student.getAge());
        });

        //编写程序，按照命令行输入的内容，完成针对数据库的随机查询，能够根据命令行输入的年龄，给出满足条件的全部成员的信息。
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入查询的年龄:");
        double age = scanner.nextDouble();
        List<Student> stus = studentMapper.queryStudentByAge(age);
        int count = 0;
        for (Student student : stus) {
            count++;
            System.out.println(count+"."+student.getName()+","+student.getSex());

        }
        System.out.println("共计"+count+"个");
    }
}
