package com.mars.day2.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DerbyConfig {
    public Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection("jdbc:derby://127.0.0.1:1527//山东工商学院;create=true;");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
}
