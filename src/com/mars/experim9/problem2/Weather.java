package com.mars.experim9.problem2;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Weather {
    WeatherForecast forecast;

    public void setForecast(WeatherForecast forecast) {
        this.forecast = forecast;
    }

    public void showWeather(){
        //设置当前日期
        Date date = new Date();
        String strDateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        System.out.print(sdf.format(date)+"   ");

        //播报天气预报
        forecast.BroadcastWeather();
    }
}
