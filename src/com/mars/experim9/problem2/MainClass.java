package com.mars.experim9.problem2;

public class MainClass {
    public static void main(String[] args) {
        Weather weather = new Weather();

        //����
        weather.setForecast(new WeatherForecast() {
            @Override
            public void BroadcastWeather() {
                System.out.println("today is raining");
            }
        });
        weather.showWeather();

        //����
        weather.setForecast(new WeatherForecast() {
            @Override
            public void BroadcastWeather() {
                System.out.println("today is sunny");
            }
        });
        weather.showWeather();

        //��ѩ��
        weather.setForecast(new WeatherForecast() {
            @Override
            public void BroadcastWeather() {
                System.out.println("today is snowy");
            }
        });
        weather.showWeather();
    }
}
