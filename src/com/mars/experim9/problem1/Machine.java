package com.mars.experim9.problem1;

public class Machine {
    Goods goods;

    public void CheckBag(Goods goods) throws DangerException{
        if (goods.isDanger()){
            DangerException danger = new DangerException();
            throw  danger;
        }
    }
}
