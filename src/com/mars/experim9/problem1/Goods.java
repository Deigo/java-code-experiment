package com.mars.experim9.problem1;

public class Goods {
    private String name;
    private boolean isDanger;

    public Goods(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public boolean isDanger() {
        return isDanger;
    }

    public void setDanger(boolean danger) {
        isDanger = danger;
    }
}
