package com.mars.experim9.problem1;

public class MainClass {
    public static void main(String[] args) {
        Machine machine = new Machine();
        //创建苹果和炸药，苹果为安全，炸药为危险，
        Goods apple = new Goods("苹果");
        apple.setDanger(false);

        Goods explosive = new Goods("炸药");
        explosive.setDanger(true);

        // 通过try-catch语句的try部分，让Machine类的实例checkBag（Goods goods）方法，查看苹果和炸药，
        try {
            machine.CheckBag(explosive);
            System.out.println(explosive.getName()+"检查通过");
        } catch (DangerException e) {
            // 一旦发现危险品，就在try-catch语句的catch部分处理危险品。
            e.toShow();
            System.out.println(explosive.getName()+"被禁止！");
        }

        try {
            machine.CheckBag(apple);
            System.out.println(apple.getName()+"检查通过");
        } catch (DangerException e) {
            e.toShow();
            System.out.println(apple.getName()+"被禁止！");
        }



    }
}
