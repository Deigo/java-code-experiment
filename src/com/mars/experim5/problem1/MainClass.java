package com.mars.experim5.problem1;

public class MainClass {
    public static void main(String[] args) {

        // 创建两个坦克，并分别设置为速度为0炮弹为10，速度为1炮弹为10；
        TanK a = new TanK(10,0);
        TanK b = new TanK(10,1);

        //	输出当前两个坦克炮弹的数量；
        System.out.println("坦克a的炮弹数量：" +a.getShellNum());
        System.out.println("坦克b的炮弹数量：" +b.getShellNum());

        //	将第一个坦克加速80，第二个坦克加速90，分别输出坦克的速度；
        a.accelerate(80);
        System.out.println("坦克a的当前速度："+a.getSpeed());
        b.accelerate(90);
        System.out.println("坦克b的当前速度："+b.getSpeed());

        //将第一个坦克减速15，第二个坦克减速30，分别输出坦克的速度；
        a.slowDown(15);
        System.out.println("坦克a的当前速度："+a.getSpeed());
        b.slowDown(30);
        System.out.println("坦克b的当前速度："+b.getSpeed());

        //通过第一个炮弹开火一次，通过第二个炮弹开火两次，分别输出当前坦克的炮弹数量。
        System.out.println("坦克a开火");
        a.fire();
        System.out.println("坦克a当前坦克的炮弹数量："+a.getShellNum());
        System.out.println("坦克b开火");
        b.fire();
        b.fire();
        System.out.println("坦克b当前坦克的炮弹数量："+b.getShellNum());

    }
}
