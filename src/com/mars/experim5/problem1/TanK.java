package com.mars.experim5.problem1;

//坦克
public class TanK {
    private int shellNum; //炮弹数量
    private int speed;    //速度

    public TanK(int shellNum, int speed) {
        this.shellNum = shellNum;
        this.speed = speed;
    }

    public int getShellNum() {
        return shellNum;
    }
    public void setShellNum(int shellNum) {
        this.shellNum = shellNum;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    //坦克加速
    public void accelerate(int accelerateSpeed){
        this.speed = this.speed + accelerateSpeed;
    }

    //坦克减速
    public void slowDown(int slowDownSpeed){
        this.speed = this.speed - slowDownSpeed;
    }

    //开火
    public void fire(){
        this.shellNum--;
        System.out.println("已发出一发炮弹");
    }

}
