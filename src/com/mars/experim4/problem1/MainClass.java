package com.mars.experim4.problem1;

public class MainClass {

    //算数运算符、关系运算符、逻辑运算符、赋值运算符、位运算符、instanceof运算符
    public static void main(String[] args) {

        //1.算数运算符 赋值运算符
        int num1,num2,endNum;
        num1 = 200;
        num2 = 100 ;
        System.out.println("算数运算符测试  赋值运算符 num1="+num1+",num2="+num2);
        endNum = num1 + num2;
        System.out.println("num1 + num2 ="+endNum);
        endNum = num1 - num2;
        System.out.println("num1 - num2 ="+endNum);
        endNum = num1 * num2;
        System.out.println("num1 * num2 ="+endNum);
        endNum = num1 / num2;
        System.out.println("num1 / num2 ="+endNum);
        //关系运算符 逻辑运算符
        System.out.println("关系运算符 逻辑运算符测试");
        if (num1 > num2){
            System.out.println("num1大于num2");
        }else if(num1 < num2){
            System.out.println("num1小于num2");
        }else {
            System.out.println("num1与num2相等");
        }
        //位运算符
        System.out.println("关系运算符 逻辑运算符测试,二进制数:num1="+Integer.toBinaryString(num1)+",num2="+Integer.toBinaryString(num2));
        System.out.println("num1 & num2 = "+Integer.toBinaryString(num1&num2));
        System.out.println("num1 | num2 = "+Integer.toBinaryString(num1|num2));
        //instanceof运算符
        System.out.println("instanceof运算符");
        Student student = new Student();
        if ( student instanceof Student){
            System.out.println("student对象是Student类创建的");
        }
    }
}
