package com.mars.experim4.problem2.test4;
public class ElectricityCharge {
    public static double getCharge(double electricity) {
       if (electricity <= 90 ){
           return electricity * 0.6;
       }
       if (electricity>90 && electricity <= 150){
           return 90 * 0.6 + (electricity - 90) * 1.1;
       }
       if (electricity > 150){
           return 90 * 0.6 + 60 * 1.1 + (electricity - 150) * 1.7;
       }
       return 0.0;
    }
}
