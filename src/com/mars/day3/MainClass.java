package com.mars.day3;


import com.mars.day3.dao.WorkerMapper;
import com.mars.day3.frame.MainFrame;
import com.mars.day3.pojo.Worker;
import com.mars.day3.utils.DateUtil;

public class MainClass {
    public static void main(String[] args) {
        initWorkTable();
        new MainFrame();
    }
    private static void initWorkTable() {
        WorkerMapper workerMapper = new WorkerMapper();
        if (workerMapper.createTable()){
            System.out.println("数据表已创建成功");
            Worker worker = new Worker();
            worker.setId("1");
            worker.setName("王林");
            worker.setSex("男");
            worker.setAddress("吉林");
            worker.setBirth(DateUtil.StringToDate("1976-12-12"));
            worker.setSalary(6654);
            workerMapper.addWorker(worker);

            worker.setId("2");
            worker.setName("翠花");
            worker.setSex("女");
            worker.setAddress("北京");
            worker.setBirth(DateUtil.StringToDate("1982-10-12"));
            worker.setSalary(1654);
            workerMapper.addWorker(worker);


            worker.setId("3");
            worker.setName("花翠");
            worker.setSex("女");
            worker.setAddress("大连");
            worker.setBirth(DateUtil.StringToDate("1984-12-12"));
            worker.setSalary(5654);
            workerMapper.addWorker(worker);

            worker.setId("4");
            worker.setName("林王");
            worker.setSex("男");
            worker.setAddress("上海");
            worker.setBirth(DateUtil.StringToDate("1978-10-12"));
            worker.setSalary(2654);
            workerMapper.addWorker(worker);
        }
    }
}
