package com.mars.day3.utils;

import javax.print.attribute.standard.DateTimeAtCompleted;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static Date StringToDate(String dateStr){
        Date date = null;
        if (dateStr == null || "".equals(dateStr))
            return null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String DateToString(Date date){
        SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd");
        return time.format(date);
    }
}
