package com.mars.day3.dao;



import com.mars.day3.config.DerbyConfig;
import com.mars.day3.pojo.Worker;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class WorkerMapper {
    private Connection connection;
    private Statement statement;
    public WorkerMapper() {
        DerbyConfig derbyConfig = new DerbyConfig();
        this.connection = derbyConfig.getConnection();
        try {
            this.statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public boolean createTable(){
        String sql = "create TABLE 职员表 (id char(6) primary key,姓名 varchar(10),性别 char(6),出生 date,地址 varchar(50),工资 float)";
        try {
            this.updateSQL(sql);
            return true;
        } catch (SQLException e) {
            System.out.println("数据库已创建，职员表创建取消");
            return false;
        }
    }

    public void addWorker(Worker worker){
        String sql1 ="insert into 职员表(id,姓名,性别,出生,地址,工资) VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(1,worker.getId());
            pst.setString(2,worker.getName());
            pst.setString(3,worker.getSex());
            pst.setDate(4,new Date(worker.getBirth().getTime()));
            pst.setString(5,worker.getAddress());
            pst.setFloat(6,worker.getSalary());
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Worker queryWorkerById(String id){
        Worker worker = null;
        String sql1 ="select * from 职员表 where id = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(1,id);
            ResultSet resultSet = pst.executeQuery();
            while (resultSet.next()){
               worker = new Worker();//id,姓名,性别,出生,地址,工资
               worker.setId(resultSet.getString("id"));
               worker.setName(resultSet.getString("姓名"));
               worker.setSex(resultSet.getString("性别"));
               worker.setBirth(new java.util.Date(resultSet.getDate("出生").getTime()));
               worker.setAddress(resultSet.getString("地址"));
               worker.setSalary(resultSet.getFloat("工资"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return worker;
    }

    public void updateWorker(Worker worker){
        String sql1 ="update 职员表 set 姓名=?,性别=?,出生=?,地址=?,工资=? where id = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql1);
            pst.setString(6,worker.getId());
            pst.setString(1,worker.getName());
            pst.setString(2,worker.getSex());
            pst.setDate(3,new Date(worker.getBirth().getTime()));
            pst.setString(4,worker.getAddress());
            pst.setFloat(5,worker.getSalary());
            pst.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public void deleteWorkerById(String id){
        String sql = "DELETE from 职员表 WHERE id = ?";
        try {
            PreparedStatement pst = this.connection.prepareStatement(sql);
            pst.setString(1,id);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Worker> selectAll(){
        ArrayList<Worker> workers = new ArrayList<>();
        String sql = "select  * from 职员表";
        try {
            ResultSet resultSet = this.querySQL(sql);
            while (resultSet.next()){
                Worker worker = new Worker();//id,姓名,性别,出生,地址,工资
                worker.setId(resultSet.getString("id"));
                worker.setName(resultSet.getString("姓名"));
                worker.setSex(resultSet.getString("性别"));
                worker.setBirth(new java.util.Date(resultSet.getDate("出生").getTime()));
                worker.setAddress(resultSet.getString("地址"));
                worker.setSalary(resultSet.getFloat("工资"));
                workers.add(worker);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return workers;
    }

    public void updateSQL(String sql) throws SQLException {
        this.statement.executeUpdate(sql);
    }

    public ResultSet querySQL(String sql) throws SQLException {
        return this.statement.executeQuery(sql);
    }

}
