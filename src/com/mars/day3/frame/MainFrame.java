package com.mars.day3.frame;

import com.mars.day3.dao.WorkerMapper;
import com.mars.day3.pojo.Worker;
import com.mars.day3.utils.DateUtil;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Vector;

/**
 * //2.3.3 任务3
 *         //能够根据用户需求，展示数据库中，当前全部的员工信息。
 *
 *         //2.3.4 任务4
 *         //通过GUI界面操作，能够根据用户输入的信息，向数据库中增加条目。
 */
public class MainFrame extends JFrame {

    //数据显示表格
    private Vector data;
    private  WorkerMapper workerMapper;
    private  JTable jTable;
    private JPanel pTable;
    private DefaultTableModel model;

    //操作提示栏
    private JPanel opPanel;
    private JLabel opLable;
    private JTextField opTextField;

    //操作按钮
    private JPanel pBtn;
    private JButton addBtn;
    private JButton deleteBtn;
    private JButton queryBtn;
    private JButton updateBtn;
    private JButton updateDataBtn;


    public MainFrame() throws HeadlessException {

        setTitle("员工表操作");

        setSize(800,600);
        setLocationRelativeTo(null);
        pTable= new JPanel();
        workerMapper = new WorkerMapper();
        initTable();
        add(pTable,BorderLayout.NORTH);
        opPanel = new JPanel();
        opLable = new JLabel();
        opTextField = new JTextField(40);
        opTextField.setEditable(false);
        opPanel.setLayout(new FlowLayout());
        opLable.setText("操作信息：");
        opPanel.add(opLable);
        opPanel.add(opTextField);
        add(opPanel,BorderLayout.CENTER);

        pBtn = new JPanel();
        addBtn = new JButton("添加员工");
        deleteBtn = new JButton("删除员工");
        queryBtn = new JButton("查询员工");
        updateBtn = new JButton("更新员工");
        updateDataBtn = new JButton("刷新");

        pBtn.add(addBtn);
        pBtn.add(deleteBtn);
        pBtn.add(queryBtn);
        pBtn.add(updateBtn);
        pBtn.add(updateDataBtn);
        add(pBtn,BorderLayout.SOUTH);
        opTextField.setText("数据加载成功");
        setBtnsListener();
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序

    }

    private void initTable() {
        model = new DefaultTableModel();

        Vector<String> name = new Vector<>();
        name.add("id");
        name.add("姓名");
        name.add("性别");
        name.add("出生");
        name.add("地址");
        name.add("工资");
        data = new Vector();
        model.setDataVector(data,name);
        jTable = new JTable(model);

        updateData();
        pTable.add(new JScrollPane(jTable));

    }

    private void setBtnsListener() {
        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                JTextField nameInput = new JTextField();
                JTextField sexInput = new JTextField();
                JTextField birthInput = new JTextField();
                JTextField addressInput = new JTextField();
                JTextField salaryInput = new JTextField();

                //"id","姓名","性别","出生","地址","工资"}
                Object[] message1 = {
                        "id:",idInput,
                        "姓名",nameInput,
                        "性别",sexInput,
                        "出生",birthInput,
                        "地址",addressInput,
                        "工资",salaryInput
                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Worker worker = new Worker();
                    worker.setId(idInput.getText());
                    worker.setName(nameInput.getText());
                    worker.setSex(sexInput.getText());
                    worker.setBirth(DateUtil.StringToDate(birthInput.getText()));
                    worker.setAddress(addressInput.getText());
                    worker.setSalary(Float.valueOf(salaryInput.getText()));
                    workerMapper.addWorker(worker);
                    opTextField.setText("添加成功");
                    updateData();
                }
            }
        });

        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                //"id","姓名","性别","出生","地址","工资"}
                Object[] message1 = {
                        "id:",idInput,

                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    workerMapper.deleteWorkerById(idInput.getText());
                    updateData();
                    opTextField.setText("删除成功");
                }
            }
        });

        updateBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                JTextField nameInput = new JTextField();
                JTextField sexInput = new JTextField();
                JTextField birthInput = new JTextField();
                JTextField addressInput = new JTextField();
                JTextField salaryInput = new JTextField();

                //"id","姓名","性别","出生","地址","工资"}
                Object[] message1 = {
                        "需要更新的id:",idInput,
                        "姓名更新为",nameInput,
                        "性别更新为",sexInput,
                        "出生更新为",birthInput,
                        "地址更新为",addressInput,
                        "工资更新为",salaryInput
                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Worker worker = new Worker();
                    worker.setId(idInput.getText());
                    worker.setName(nameInput.getText());
                    worker.setSex(sexInput.getText());
                    worker.setBirth(DateUtil.StringToDate(birthInput.getText()));
                    worker.setAddress(addressInput.getText());
                    worker.setSalary(Float.valueOf(salaryInput.getText()));
                    workerMapper.updateWorker(worker);
                    updateData();
                    opTextField.setText("更新成功");
                }
            }
        });
        queryBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JTextField idInput = new JTextField();
                //"id","姓名","性别","出生","地址","工资"}
                Object[] message1 = {
                        "输入查询id:",idInput,

                };

                int option = JOptionPane.showConfirmDialog(null, message1);
                if (JOptionPane.OK_OPTION == option){
                    Worker result = workerMapper.queryWorkerById(idInput.getText());
                    if (result != null){
                        String str = "查询成功："+result.getId()+"|"+result.getName()+"|"+result.getSex()+"|"
                                +DateUtil.DateToString(result.getBirth())+"|"+result.getAddress()+"|"+result.getSalary();
                        JLabel jLabel= new JLabel();
                        jLabel.setText(str);
                       opTextField.setText(str);
                    }

                }
            }
        });
        updateDataBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("数据已更新");
                updateData();
            }
        });
    }

    private void updateData() {
        List<Worker> workers = this.workerMapper.selectAll();
        this.data.clear();
        for (Worker worker : workers) {
            Vector<String> row = new Vector<>();
            row.add( worker.getId());
            row.add(worker.getName());
            row.add(worker.getSex());
            row.add(DateUtil.DateToString(worker.getBirth()));
            row.add(String.valueOf(worker.getAddress()));
            row.add(String.valueOf(worker.getSalary()));
            data.add(row);
        }
        this.repaint();
        this.jTable.updateUI();
    }

}
