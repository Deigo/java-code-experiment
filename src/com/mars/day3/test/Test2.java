package com.mars.day3.test;

import java.io.*;

public class Test2 {
    public static void main(String[] args) {
        File file = new File("c:/last/test.txt");
        BufferedWriter bufferedWriter = null;
        try {
//            bufferedWriter = new BufferedWriter(new BufferedWriter(new FileWriter(file)));
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String str = "";
            while ((str = bufferedReader.readLine())!=null){
                System.out.println(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String  str = "test";
        try {
            bufferedWriter.newLine();
            bufferedWriter.write(str);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
