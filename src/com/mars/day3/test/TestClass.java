package com.mars.day3.test;

import com.mars.day3.dao.WorkerMapper;
import com.mars.day3.pojo.Worker;

import java.util.Date;

public class TestClass {
    public static void main(String[] args) {

        initWorkTable();//初始化员工表
    }

    private static void initWorkTable() {
        WorkerMapper workerMapper = new WorkerMapper();
        workerMapper.createTable();
        System.out.println("数据表已创建");
        Worker worker = new Worker();
        worker.setId("1");
        worker.setName("张三");
        worker.setSex("男");
        worker.setAddress("莱山");
        worker.setBirth(new Date());
        worker.setSalary(4800);
        workerMapper.addWorker(worker);

        worker.setId("2");
        worker.setName("李四");
        worker.setSex("男");
        worker.setAddress("莱山");
        worker.setBirth(new Date());
        worker.setSalary(6200);
        workerMapper.updateWorker(worker);


        worker.setId("3");
        worker.setName("王五");
        worker.setSex("男");
        worker.setAddress("莱山");
        worker.setBirth(new Date());
        worker.setSalary(3700);
        workerMapper.updateWorker(worker);
    }
}
