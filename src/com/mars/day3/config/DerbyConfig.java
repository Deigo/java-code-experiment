package com.mars.day3.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DerbyConfig {
    public Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection("jdbc:derby://127.0.0.1:1527//移动公司2;create=true;");
        } catch (SQLException throwables) {
            System.out.println("数据库连接失败，请确保已配置Derby数据库，并开启startNetworkServer");
            throwables.printStackTrace();
        }
        return connection;
    }
}
