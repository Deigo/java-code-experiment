package com.mars.experim11.problem3;

import java.util.List;

//模拟业务层
public class DemoService {

    private ServerDemo serverDemo;  //持久层操作

    public DemoService() {

        this.serverDemo = new ServerDemo(); //注入持久层操作
    }

    public void addUnitInfo(UnitInfo info) {
        this.serverDemo.saveUnitInfo(info);
    }

    public List<UnitInfo> uploadData() {
        return this.serverDemo.getUnitInfos();
    }

    public void undateUnitInfo(UnitInfo info) {
        this.serverDemo.saveUnitInfo(info);
    }

    public void deleteUnitInfoByName(String name) {
        this.serverDemo.deleteUnitInfoByName(name);
    }
}
