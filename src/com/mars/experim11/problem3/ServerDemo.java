package com.mars.experim11.problem3;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

//持久层模拟
public class ServerDemo {

    private final String FILE_PATH = "C:\\ch10";  //物资供给单位信息根目录，模拟数据库连接
    private File[] unitInfoFiles;

    public ServerDemo() {
        File unitInoFile = new File(FILE_PATH);
        this.unitInfoFiles = unitInoFile.listFiles();
    }

    public void saveUnitInfo(UnitInfo info){
        try {
            String fileName = info.getName()+new String("单位信息".getBytes("GBK"), "UTF-8")+".txt";
            File file = new File(FILE_PATH + "\\" + fileName);
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
            String  str = info.getProperty()+" "+info.getName()+" "+info.getPhone()+" "+info.getAddress();
            System.out.println(str);
            bufferedWriter.write(str);
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<UnitInfo> getUnitInfos() {
        File unitInoFile = new File(FILE_PATH);
        this.unitInfoFiles = unitInoFile.listFiles();
        ArrayList<UnitInfo> unitInfos = new ArrayList<>();
        try {
            for (File infoFile : unitInfoFiles) {
                UnitInfo unitInfo = GetUnitInfoByfile(infoFile);
                unitInfos.add(unitInfo);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return unitInfos;
    }

    private static UnitInfo GetUnitInfoByfile(File file) throws IOException {
        String str = null;
        UnitInfo unitInfo = new UnitInfo();
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        while ((str = bufferedReader.readLine())!=null){
            String[] s = str.split(" ");
            unitInfo.setProperty(s[0]);
            unitInfo.setName(s[1]);
            unitInfo.setPhone(s[2]);
            unitInfo.setAddress(s[3]);
        }
        fileReader.close();
        bufferedReader.close();
        return unitInfo;
    }

    public void deleteUnitInfoByName(String name) {
        String fileName = null;
        try {
            fileName = name+new String("单位信息".getBytes("GBK"), "UTF-8")+".txt";
            File file = new File(FILE_PATH + "\\" + fileName);
            file.delete();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
