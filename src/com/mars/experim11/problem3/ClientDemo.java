package com.mars.experim11.problem3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//前端模拟    数据的增删查改
public class ClientDemo {

    private List<UnitInfo> unitInfos; //存储物资供给单位信息

    private DemoService demoService; //业务层对象

    public ClientDemo() {
        this.unitInfos = new ArrayList<>();
        this.demoService = new DemoService();
        this.unitInfos = this.demoService.uploadData();
    }

    //模拟前端控制中心
    public void menu(){
        Scanner scanner = new Scanner(System.in);
        while (true){
            System.out.println("操作：" +"\n"+
                            "1.查看存所有储物资供给单位信息" +"\n"+
                            "2.根据物资供给单位查看"+"\n"+
                            "3.增加储物资供给单位信息" +"\n"+
                            "4.更新储物资供给单位信息" +"\n"+
                            "5.删除储物资供给单位信息" +"\n"+
                            "0.退出");
            System.out.print("请输入：");
            int order = scanner.nextInt();
            switch (order){
                case 1:showUnitInfos();break;
                case 2:showUnitInfoByName();break;
                case 3:addUnitInfo();break;
                case 4:updateUnitInfo();break;
                case 5:deleteUnitInfo();break;
                case 0:return;
            }
        }

    }

    private void deleteUnitInfo() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入删除单位名称：");
        String name = scanner.next();
        for (UnitInfo unitInfo : this.unitInfos) {
            if (name.equals(unitInfo.getName())){
                this.unitInfos.remove(new UnitInfo(name));
                this.demoService.deleteUnitInfoByName(name);
                return ;
            }
        }
        System.out.println("该单位不存在");
    }

    private void updateUnitInfo() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入更新单位名称：");
        String name = scanner.next();
        for (UnitInfo unitInfo : this.unitInfos) {
            if (name.equals(unitInfo.getName())){
                UnitInfo updateInfo = new UnitInfo();
                System.out.print("请输入更新单位性质：");
                updateInfo.setProperty(scanner.next());
                updateInfo.setName(name);
                System.out.print("请输入更新单位电话：");
                updateInfo.setPhone(scanner.next());
                System.out.print("请输入更新单位地址：");
                updateInfo.setAddress(scanner.next());
                this.demoService.undateUnitInfo(updateInfo);
                return;
            }
        }
        System.out.println("该单位不存在");
    }

    private void addUnitInfo() {
        Scanner scanner = new Scanner(System.in);
        UnitInfo info = new UnitInfo();
        System.out.print("请输入添加单位性质：");
        info.setProperty(scanner.next());
        System.out.print("请输入添加单位名称：");
        info.setName(scanner.next());
        System.out.print("请输入添加单位电话：");
        info.setPhone(scanner.next());
        System.out.print("请输入添加单位地址：");
        info.setAddress(scanner.next());
        this.demoService.addUnitInfo(info);
    }

    private void showUnitInfoByName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入查看单位名称：");
        String name = scanner.next();
        for (UnitInfo unitInfo : this.unitInfos) {
            if (name.equals(unitInfo.getName())){
                System.out.println(unitInfo);
                return;
            }
        }
        System.out.println("该单位信息不存在");
    }

    private void showUnitInfos() {
        this.unitInfos = this.demoService.uploadData();
        unitInfos.forEach(unitInfo -> {
            System.out.println(unitInfo);
        });
    }

}
