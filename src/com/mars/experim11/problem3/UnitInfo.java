package com.mars.experim11.problem3;

//物资供给单位信息
//1）创建一个信息存储类，属性为：“单位性质”，“单位名称”、“联系电话”、“单位地址”；
public class UnitInfo {
    private String property; //单位性质
    private String name; //单位名称
    private String phone; //联系电话
    private  String address; //单位地址

    public UnitInfo() {
    }

    public UnitInfo(String name) {
        this.name = name;
    }

    public UnitInfo(String property, String name) {
        this.property = property;
        this.name = name;
    }

    public UnitInfo(String property, String name, String phone, String address) {
        this.property = property;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return
                "性质：" + property +
                ", 名称：" + name +
                ", 联系方式：" + phone +
                ", 地址：" + address
                ;
    }
}
