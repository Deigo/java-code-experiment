package com.mars.experim11.problem2;

import java.io.File;

public class MainClass {
    public static void main(String[] args) {
        String path = "C:\\ch10"; // 路径
        File f = new File(path);
        if (!f.exists()) {
            System.out.println(path + " 该路径不存在");
            return;
        }
        File[] files = f.listFiles();
        for (int i = 0; i < files.length; i++) {
            final String FILE_NAME = "txt";
            File file = files[i];
            String fileName = file.getName();
            String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (FILE_NAME.equals(suffix)){
                System.out.println(fileName);
            }

        }
    }
}
