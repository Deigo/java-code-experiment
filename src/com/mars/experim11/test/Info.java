package com.mars.experim11.test;


public class Info {
    private String property;
    private String name;
    private String phone;
    private  String address;


    public Info() {
    }

    public Info(String property, String name, String phone, String address) {
        this.property = property;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }
    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Info{" +
                "property='" + property + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
