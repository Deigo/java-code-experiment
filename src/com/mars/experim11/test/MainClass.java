package com.mars.experim11.test;

import java.io.*;

public class MainClass {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String fileName = "C:\\ch10\\山东工商学院单位信息.txt";
        File file = new File(fileName);
        BufferedWriter bWriter = null;
        BufferedReader bReader = null;
        SchoolInfo sdtbu = new SchoolInfo("学校", "山东工商学院", "13546467889", "莱山区");
        try {

            bWriter = new BufferedWriter(new FileWriter(file));
            String writeStr = sdtbu.getProperty()+" "+ sdtbu.getName()+" "+sdtbu.getPhone()+" "+sdtbu.getAddress();
            bWriter.write(writeStr);
            bWriter.close();

            bReader  = new BufferedReader(new FileReader(file));
            String readStr =null;
            SchoolInfo readInfo = new SchoolInfo();

            while ((readStr =bReader.readLine())!=null){
                String[] split = readStr.split("\\s+");//分割一个或多个空格
                readInfo.setProperty(split[0]);
                readInfo.setName(split[1]);
                readInfo.setPhone(split[2]);
                readInfo.setAddress(split[3]);
                System.out.println("读取内容为："+readInfo);
            }
            bReader.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

class SchoolInfo{
    String property;
    String name;
    String phone;
    String address;

    public SchoolInfo() {
    }

    public SchoolInfo(String property, String name, String phone, String address) {
        this.property = property;
        this.name = name;
        this.phone = phone;
        this.address = address;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "schoolInfo{" +
                "property='" + property + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
