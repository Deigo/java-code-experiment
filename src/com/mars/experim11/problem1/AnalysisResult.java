package com.mars.experim11.problem1;

import java.io.*;

public class AnalysisResult {
    public static void main(String[] args) {
        File fRead = new File("score.txt"); //读取当前目录下文件score.txt
        File fWriter = new File("scoreAnalysis.txt");//读取当前目录下文件scoreAnalysis.txt
        try{
            Writer out = new FileWriter(fWriter,true); //以尾加方式穿甲指向文件fWriter的out流
            BufferedWriter bufferedWriter = new BufferedWriter(out); //创建指向out的buffferWrieter流
            Reader in = new FileReader(fRead); // 创建指向文件fREad的in流
            BufferedReader bufferedReader = new BufferedReader(in); // 创建指向in的bufferRead流
            String str = null;
            while ((str = bufferedReader.readLine())!=null){  //读取score中的一行字符串，若为空则退出循环
                double totalScore = Fenxi.getTotalScore(str);  //根据一行字符串获取totalScore
                str = str + " 总分：" +totalScore; //将获取到的totalScore 拼接为  总分：totalScore
                System.out.println(str);  //打印到控制台拼接上总分后的 当前行数据
                bufferedWriter.write(str); //将拼接后的字符串通过bufferedWriter写入scoreAnalysis.txt
                bufferedWriter.newLine();//将写入光标移动到下一行
            }
            bufferedReader.close(); //关闭字节码读取流
            bufferedWriter.close();//关闭字节码写入流
        } catch (IOException e) {//捕获IO异常并打印错误信息
            System.out.println(e.toString());
        }
    }
}
