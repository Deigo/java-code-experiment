package com.mars.experim11.problem1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Fenxi {

    /**
     * 根据个人所有成绩，计算总分
     * @param str 一行成绩
     * @return totalScore 总分
     */
    public static double getTotalScore(String str) {

        Scanner scanner = new Scanner(str); //创建Scanner对象，输入源为传入字符串
        scanner.useDelimiter("[^0123456789.]+"); //将此扫描器的分隔模式设置为从指定的构造的模式 String,即非数字作为分隔
        double totalScore = 0; //声明总分变量
        while (scanner.hasNext()){ //字符串中是否还有未读取的值，没有则退出循环
            try {
                double score = scanner.nextDouble(); //获取字符串中的一个分数存入score中
                totalScore = totalScore +score; //累加获取总分
            }catch (InputMismatchException e){//捕获是输入不匹配异常
                String t = scanner.next();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return totalScore;
    }
}
