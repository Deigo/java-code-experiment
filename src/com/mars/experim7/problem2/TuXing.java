package com.mars.experim7.problem2;

import java.util.Map;

public abstract class TuXing {

    protected Map<String,Double> edge; //边长,用于存放图形的多种边长信息

    public abstract  double getArea();
}
