package com.mars.experim7.problem2;


import java.util.HashMap;

//矩形
public class Rect extends TuXing {

    public Rect(double length,double width) {
        super.edge = new HashMap<>();
        super.edge.put("length",length);
        super.edge.put("width",width);
    }

    @Override
    public double getArea() {
        Double length = super.edge.get("length");
        Double width = super.edge.get("width");
        return length * width;
    }
}
