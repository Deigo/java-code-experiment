package com.mars.experim7.problem2;

import java.util.ArrayList;
import java.util.List;

public class TotalArea {

    private List<TuXing> tuXings; //图形集合 用于存放多个图形
    private double areaSum;  //所有图形面积之和

    public TotalArea() {
        if (this.tuXings == null){
            this.tuXings = new ArrayList<>();
        }
        this.areaSum = 0;
    }

    //添加图形
    public void addTuXing(TuXing tuXing){
        tuXings.add(tuXing);
    }

    //计算所有图形面积之和
    public double computerTotalArea(){
        //遍历添加的所有图形
        this.tuXings.forEach(tuXing -> {
            //获取图形的面积放入areaSum中
            this.areaSum += tuXing.getArea();
        });
        return areaSum;
    }
}
