package com.mars.experim7.problem2;

public class MainClass {
    public static void main(String[] args) {
        TotalArea totalArea = new TotalArea();

       // 建立15个圆和15个矩形
        totalArea.addTuXing(new Circle(1.0));
        totalArea.addTuXing(new Circle(2.0));
        totalArea.addTuXing(new Circle(3.0));
        totalArea.addTuXing(new Circle(4.0));
        totalArea.addTuXing(new Circle(5.0));
        totalArea.addTuXing(new Circle(6.0));
        totalArea.addTuXing(new Circle(7.0));
        totalArea.addTuXing(new Circle(8.0));
        totalArea.addTuXing(new Circle(9.0));
        totalArea.addTuXing(new Circle(10.0));
        totalArea.addTuXing(new Circle(11.0));
        totalArea.addTuXing(new Circle(12.0));
        totalArea.addTuXing(new Circle(13.0));
        totalArea.addTuXing(new Circle(14.0));
        totalArea.addTuXing(new Circle(15.0));

        totalArea.addTuXing(new Rect(3.0,4.0));
        totalArea.addTuXing(new Rect(4.0,5.0));
        totalArea.addTuXing(new Rect(5.0,6.0));
        totalArea.addTuXing(new Rect(6.0,7.0));
        totalArea.addTuXing(new Rect(7.0,8.0));
        totalArea.addTuXing(new Rect(8.0,9.0));
        totalArea.addTuXing(new Rect(9.0,10.0));
        totalArea.addTuXing(new Rect(10.0,11.0));
        totalArea.addTuXing(new Rect(11.0,12.0));
        totalArea.addTuXing(new Rect(12.0,13.0));
        totalArea.addTuXing(new Rect(13.0,14.0));
        totalArea.addTuXing(new Rect(14.0,15.0));
        totalArea.addTuXing(new Rect(15.0,16.0));
        totalArea.addTuXing(new Rect(16.0,17.0));
        totalArea.addTuXing(new Rect(17.0,18.0));

        double areaSum = totalArea.computerTotalArea();
        System.out.println("所有图形之和为："+areaSum);
    }
}
