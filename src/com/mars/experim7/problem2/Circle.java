package com.mars.experim7.problem2;


import java.util.HashMap;
//圆形
public class Circle extends TuXing {

    private final Double PI = 3.14;

    public Circle(Double radius) {
        super.edge = new HashMap<>();
        super.edge.put("radius",radius);
    }

    @Override
    public double getArea() {
        Double radius = super.edge.get("radius");
        return radius*radius*PI;
    }
}
