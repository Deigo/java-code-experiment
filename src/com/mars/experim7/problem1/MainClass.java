package com.mars.experim7.problem1;

public class MainClass {
    public static void main(String[] args) {

        ChinaPeople chinaPeople = new ChinaPeople(180,150);
        chinaPeople.speakHello();
        chinaPeople.averageHeight();
        chinaPeople.averageWeight();
        chinaPeople.chinaGongfu();

        AmericanPeople americanPeople = new AmericanPeople(175,135);
        americanPeople.speakHello();
        americanPeople.averageHeight();
        americanPeople.averageWeight();
        americanPeople.americanBoxing();

        BeijingPeople beijingPeople = new BeijingPeople(190,160);
        beijingPeople.speakHello();
        beijingPeople.averageHeight();
        beijingPeople.averageWeight();
        beijingPeople.BeijingOpera();
    }
}
