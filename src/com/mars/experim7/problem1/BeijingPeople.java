package com.mars.experim7.problem1;

public class BeijingPeople extends People {
    public BeijingPeople(double height, double weight) {
        super(height, weight);
    }

    public void BeijingOpera(){
        System.out.println("花脸、青衣");
    }

    @Override
    public void speakHello() {
        System.out.println("我是BeijingPeople，你好");
    }

    @Override
    public void averageHeight() {
        System.out.println("BeijingPeople的Height："+super.height);
    }

    @Override
    public void averageWeight() {
        System.out.println("BeijingPeople的Weight："+super.weight);
    }
}
