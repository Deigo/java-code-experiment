package com.mars.experim7.problem1;

public class People {
    protected double height;
    protected double weight;

    public People(double height, double weight) {
        this.height = height;
        this.weight = weight;
    }

    public void speakHello(){
        System.out.println("hello");
    }
    public void averageHeight(){
        System.out.println("height:"+this.height);
    }
    public void averageWeight(){
        System.out.println("weight:"+this.weight);
    }
}
