package com.mars.experim7.problem1;

public class AmericanPeople extends People {

    public AmericanPeople(double height, double weight) {
        super(height, weight);
    }

    public void americanBoxing(){
        System.out.println("直拳、勾拳、组合拳");
    }

    @Override
    public void speakHello() {
        System.out.println("I'm AmericanPeople，hello");
    }

    @Override
    public void averageHeight() {
        System.out.println("AmericanPeople的Height："+super.height);
    }

    @Override
    public void averageWeight() {
        System.out.println("AmericanPeople的Weight："+super.weight);
    }
}
