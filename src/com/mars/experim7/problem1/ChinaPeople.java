package com.mars.experim7.problem1;

public class ChinaPeople extends People {
    public ChinaPeople(double height, double weight) {
        super(height, weight);
    }

    public void chinaGongfu(){
        System.out.println("坐如钟、站如松、睡如弓");
    }

    @Override
    public void speakHello() {
        System.out.println("我是ChinaPeople，你好");
    }

    @Override
    public void averageHeight() {
        System.out.println("ChinaPeople的Height："+super.height);
    }

    @Override
    public void averageWeight() {
        System.out.println("ChinaPeople的Weight："+super.weight);
    }
}
