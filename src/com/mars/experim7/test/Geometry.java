package com.mars.experim7.test;

public abstract class Geometry {
    public abstract double getArea();
}
