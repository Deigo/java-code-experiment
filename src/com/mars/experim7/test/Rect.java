package com.mars.experim7.test;

public class Rect extends Geometry{
    double a,b;

    public Rect(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double getArea() {
        return this.a*this.b;
    }
}
