package com.mars.experim13;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Teacher implements ActionListener {
    int numberOne,numberTwo; //声明计算数字1，数字2

    String operator= ""; //声明运算符

    boolean isRight;

    Random random ;//用于给出随机数

    int maxInteger;//题目中最大的整数

    JTextField textOne, textTwo, textResult;  //声明显示数字1,数字2，结果的文本框
    JLabel operatorLabel, message; //声明显示运算符，运算信息的标签

    //构造函数
    Teacher() {
        random = new Random();//创建一个随机数对象

    }

    //设置参与计算的最大整数
    public void setMaxInteger(int n){
        maxInteger = n;
   }

   //监听事件实现方法
    @Override
    public void actionPerformed( ActionEvent е) {
        String str = е. getActionCommand(); //获取操作事件命令字符串
        if(str.equals("getProblem")) { //若点击获取问题按钮
            numberOne = random. nextInt( maxInteger) + 1;//1至maxInteger之间的随机数
            numberTwo = random. nextInt( maxInteger) + 1;
            double d = Math.random();//获取(0,1)之间的随机数
            if(d>= 0.5) //若d大于等于0.5则设置运算符为“+”，否则设置运算符为“-”
                operator= "+";
            else
                operator= "-";
            textOne.setText("" + numberOne) ;//将numberOne显示到文本框textOne中
            textTwo.setText("" + numberTwo) ;//将numberTwo显示到文本框textTwo中
            operatorLabel.setText(operator);//将运算符operator显示到标签operatorLabel中
            message. setText("请回答"); //设置显示信息为请回答
            textResult. setText( null);//清空结果输入框
        }
        else if(str.equals("answer")) {    //若果输入结果后敲回车，或者是点击确认答案按钮

            String answer = textResult. getText(); //获取输入结果

            try {
                int result = Integer.parseInt(answer); //将获取的输入结果，由String类型转化为Integer类型
                if (operator.equals(" +")) {   //如果操作符是“+”
                    if (result == numberOne + numberTwo) //如果结果等于数字1加数字二，则消息标签显示"你回答正确"
                        message.setText("你回答正确");
                    else                              //否则显示"你回答错误"
                        message.setText("你回答错误");
                } else if (operator.equals("-")) {//如果操作符是“-”
                    if(result == numberOne - numberTwo)//如果结果等于数字1减去数字二，则消息标签显示"你回答正确"
                        message.setText("你回答正确");
                    else                            //否则显示"你回答错误"
                        message.setText("你回答错误");

                }
            } catch(NumberFormatException x) {  //捕获数字格式异常
                    message. setText("请输人数字字符");  //消息标签显示"请输人数字字符"
         }
     }
    }

    //设置数字1，数字2，运算结果的束缚框 三个点是代表可以接受多个实际参数，这里的多个指的是不限个数，可以是一个、两个、三个甚至更多
    public void setJTextField(JTextField ... t) {
        textOne = t[0];
        textTwo = t[1];
        textResult = t[2];
    }

    //设置运算符，运算结果显示标签
    public void setJLabel(JLabel ... label) {
        operatorLabel = label[0];
        message = label[1];
    }

}



