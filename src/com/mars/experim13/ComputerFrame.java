package com.mars.experim13;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class ComputerFrame extends JFrame {
    JMenuBar menubar;    // 声明顶部菜单栏
    JMenu choiceGrade; //选择级别的菜单
    JMenuItem grade1, grade2; //声明菜单栏中的选项
    JTextField textOne, textTwo, textResult; //声明 三个文本框，其中前两个显示计算数字,后一个显示结果
    JButton getProblem, giveAnswer ; // 声明两个按钮，获取问题，给出答案
    JLabel operatorLabel, message;//声明连个标签，算术符号,计算结果信息
    Teacher teacherZhang; //声明一个Teacher对象

    //ComputerFrame构造函数，初始化画板
    ComputerFrame() {

        teacherZhang= new Teacher( ) ; //创建一个Teacher对象
        teacherZhang. setMaxInteger(20); //设置算术默认对象最大值为20
        setLayout(new FlowLayout()); //设置画本布局为FlowLayout流式布局
        menubar = new JMenuBar();  //创建一个菜单栏
        choiceGrade = new JMenu("选择级别");//创建一个菜单为"选择级别"
        grade1 = new JMenuItem("幼儿级别");//创建一个菜单选项为"幼儿级别"
        grade2 = new JMenuItem("儿童级别");//创建一个菜单选项为"儿童级别"
        grade1.addActionListener(new ActionListener(){ //給菜单选项"幼儿级别"绑定监听事件，设置最大计算为10
            @Override
            public void actionPerformed(ActionEvent e) {
                teacherZhang.setMaxInteger(10);
            }
        });

        grade2.addActionListener(new ActionListener(){//給菜单选项"儿童级别"绑定监听事件，设置最大计算为50
            @Override
            public void actionPerformed(ActionEvent e) {
                teacherZhang.setMaxInteger(50);
            }
        });

        choiceGrade. add( grade1);//将菜单选项"幼儿级别"，添加到菜单"选择级别"中
        choiceGrade. add( grade2);//将菜单选项"儿童级别"，添加到菜单"选择级别"中

        menubar.add( choiceGrade);//将菜单"选择级别"，添加到菜单栏中
        setJMenuBar(menubar);// 将菜单栏设置为画板菜单栏

        this.textOne = new JTextField(5);//创建textOne,其可见字符长是5

        textTwo = new JTextField(5);//创建textTwo,其可见字符长是5

        textResult= new JTextField(5);//创建textResult,其可见字符长是5

        operatorLabel = new JLabel("+ ");//创建操作符，初始默认值为“+”

        operatorLabel. setFont(new Font( "Arial", Font. BOLD, 20)); //设置操作符字体为Arial，加粗，字号20

        message = new JLabel("你还没有回答呢");  //创建消息显示标签，默认值为"你还没有回答呢"

        getProblem = new JButton("获取题目");//创建"获取题目"按钮
        giveAnswer = new JButton("确认答案");//创建"确认答案"按钮
        add( textOne);     //将数字1添加到画板中
        add( operatorLabel);//将算术运算符添加到画板中
        add( textTwo);      //将数字2添加到画板中
        add(new JLabel("= "));//将”=“添加到画板中

        add( textResult);//将运算结果添加到画板中

        add(giveAnswer);//将"确认答案"按钮添加到画板中

        add( message);//将消息显示标签添加到画板中

        add(getProblem);//将"获取题目"按钮添加到画板中

        textResult. requestFocus(); //请求此Component获取输入焦点

        textOne. setEditable( false) ; //设置数字1文本框为不可编辑

        textTwo. setEditable(false);//设置数字2文本框为不可编辑

        getProblem. setActionCommand( "getProblem" ); //设置"获取题目"按钮添用于操作事件的命令字符串为 "getProblem"

        textResult.setActionCommand("answer");//设置计算结果文本框添用于操作事件的命令字符串为 "answer"

        giveAnswer.setActionCommand("answer");//设置"确认答案"按钮添用于操作事件的命令字符串为 "answer"

        teacherZhang. setJTextField( textOne, textTwo,textResult);
        teacherZhang.setJLabel(operatorLabel, message);
        getProblem.addActionListener(teacherZhang);//代码2//将teacherhang注册为getProblem的Actionvent事件监视器
        giveAnswer.addActionListener(teacherZhang);// 代码3//将teacherhang注册为giveAnswer的Actionvent事件监视器
        textResult.addActionListener(teacherZhang);// 代码4 将teacherhang注册为textResult的Actionvent事件监视器
        setVisible( true); //设置画板为可见
        validate();      //验证此容器及其所有子组件。
        setDefaultCloseOperation(DISPOSE_ON_CLOSE); //设置用户在此框架上启动“关闭”时默认执行的操作，使用System exit方法退出exit程序
    }


}
